// sudo npm install -g is-running

var 	exec = require('child_process').spawn,
		fs = require('fs');
var i = -1;

check();
setInterval(function() {
		check();
}, 10000);


function check() {
	fs.readFile('ts3server.pid', 'utf8', function (err, data) {
		if (!err) {
			var pid = data.split("\n"); pid = pid[0];
			var isRunning = require('is-running')(pid);
			if (!isRunning) {
				run();
			} else {
				i = 0;
			}
		} else {
			run();
		}
	});
}

function run() {
	if (i < 3) {
		var child = exec('./ts3server_startscript.sh', ['start', 'inifile=ts3server.ini'], {
			detached: true
		});
		child.unref();
		i++;
	} else {
		exit();
	}
}

function exit() {
	process.exit();
}