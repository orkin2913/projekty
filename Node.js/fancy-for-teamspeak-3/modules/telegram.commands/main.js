'use strict';

/**
  *  Logs from Socket
  *
  *  @author     Orkin (AVNTeam.net)
  */
class TelegramCommands {

/**
  *  Class constructor
  *
  *  @author     Orkin (AVNTeam.net)
  */
  constructor( query, Config, Lang, logger, socket, telegram ) {
    // If telegram is enabled
    if ( telegram !== null ) {
      this.log = logger.getLogger( 'TELEGRAM' );
      this.log.setLevel( global.loglevel );
      this.cfg = new Config();
      this.config = this.cfg.getConfig();
      this.lang = new Lang( global.locale );
      this.main( telegram );
    }
  }

  main( telegram ) {
    var self = this;
    telegram.on( 'command', ( data, command, args ) => {
      if ( command === 'test' ) {
        if ( data.from.id === 244797767 ) telegram.sendMessage( data.chat.id, 'Orkin jest najlepszy!' );
        else telegram.sendMessage( data.chat.id, 'Nie dla psa!' );
      } else if ( command === 'info' ) {
        telegram.sendMessage( data.chat.id, 'Fancy by ' + self.lang.get( 'author' ) )
      }
    } );
  }

}

module.exports = TelegramCommands;
