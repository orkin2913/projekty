'use strict';

/**
  *  Logs from Socket
  *
  *  @author     Orkin (AVNTeam.net)
  */
class SocketLog {

/**
  *  Class constructor
  *
  *  @author     Orkin (AVNTeam.net)
  */
  constructor( query, Config, Lang, logger, socket ) {
    if ( socket !== null ) {
      this.lang = new Lang( global.locale );
      this.log = logger.getLogger( 'SOCKET' );
      this.log.setLevel( global.loglevel );
      this.main( socket );
      this.log.debug( this.lang.get( 'modules.socketlog.loaded' ) );
    }
  }

  main( socket ) {
    // Wait until socket is loaded
    socket.on( 'loaded', () => {

    } );
    // When client connect by telnet
    socket.on( 'connect', ( name ) => {
      this.log.debug( name + ' ' + this.lang.get( 'modules.socketlog.connected' ) );
    } );
    socket.on( 'disconnect', ( name ) => {
      this.log.debug( name + ' ' + this.lang.get( 'modules.socketlog.disconnected' ) );
    } );
    // When client send data
    socket.on( 'data', ( name, data ) => {
      this.log.debug( name + ' >> ' + data );
    } );
    // When post is sent
    socket.on( 'postData', ( ip, data ) => {
      this.log.debug( ip + ' >> ' + JSON.stringify( data ) );
    } );
  }

}

module.exports = SocketLog;
