'use strict';

/**
  *  Example class
  *
  *  @author     Orkin (AVNTeam.net)
  */
class Example {

/**
  *  Class constructor
  *
  *  @author     Orkin (AVNTeam.net)
  */
  constructor( query, Config, Lang, logger ) {
    this._localPath = __dirname;
    this.cfg = new Config( null, this._localPath );
    this.config = this.cfg.getConfig();
    this.lang = new Lang( this.config.module.locale, this._localPath );
    this.log = logger.getLogger( this.config.module.name );
    this.log.setLevel( this.config.module.loglevel );
    this.main( query );
    this.log.debug( this.lang.get( 'app.loaded' ) );
  }

  main( query ) {

  }

}

module.exports = Example;
