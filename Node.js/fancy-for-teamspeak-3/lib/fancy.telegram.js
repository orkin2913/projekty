'use strict';

var fs = require( 'fs' );
var path = require( 'path' );
var logger = require( 'log4js' );
var EventEmitter = require( 'events' );
var TelegramBot = require('node-telegram-bot-api');
var Lang = require( path.join( global.dir, 'lib/fancy.lang.js' ) );


/**
  *  Klasa obsługująca bota na telegramie
  *
  *  @author     Orkin (AVNTeam.net)
  */
class Telegram extends EventEmitter {

/**
  *  Konstruktor klasy
  *
  *  @author     Orkin (AVNTeam.net)
  */
  constructor( config ) {
    // http://stackoverflow.com/questions/34947427/how-to-resolve-this-is-not-defined-when-extending-eventemitter
    super();
    this._log = logger.getLogger( 'TELEGRAM' );
    this._log.setLevel( global.loglevel );
    this._config = config;
    // Ładowanie języka
    this._lang = new Lang( global.locale );
    // ładowanie Telegrama
    this._telegram = new TelegramBot( this._config.token, { polling: true, filepath: false } );
    this._commands();

    this._log.info( this._lang.get( 'telegram.loaded' ) );
  }

  sendMessage( chatID, msg ) {
    var self = this;
    self._telegram.sendMessage( chatID, msg ).catch( ( error ) => {
      self._log.error( error.response.body.description ); // => { ok: false, error_code: 400, description: 'Bad Request: chat not found' }
    } );
  }

/**
  *  Funkcja nasłuchująca komend
  *
  *  @author     Orkin (AVNTeam.net)
  */
  _commands() {
    var self = this;
    // REGEX:
    //  \/ matches the character / literally
    //  ([^@\s]+) matches the command
    //  @? matches the character @ literally
    //  (?:(\S+)|) matches the bot name
    //  \s? matches any whitespace character
    //  ([\s\S]*) matches args
    //  https://gist.github.com/sk22/cc02d95cd2d24c882835c1dddb33e1da
    self._telegram.onText( /^\/([^@\s]+)@?(?:(\S+)|)\s?([\s\S]*)$/, ( msg, match ) => {
      // Rejestruj komendy bez bezpośredniego wskazania na bota wtedy gdy opcja włączona lub gdy czat prywatny
      if ( self._config.registerAll || match[ 2 ] === self._config.botLogin || msg.from.id === msg.chat.id ) self.emit( 'command', msg, match[ 1 ], match[ 3 ] );
    } );
  }

}

module.exports = Telegram;
