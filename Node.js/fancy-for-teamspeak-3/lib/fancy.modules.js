'use strict';

var path = require( 'path' );
var logger = require( 'log4js' );
var requireAll = require( 'require-all' );
var Lang = require( path.join( global.dir, 'lib/fancy.lang.js' ) );
var Config = require( path.join( global.dir, 'lib/fancy.config.js' ) );
var Socket = require( path.join( global.dir, 'lib/fancy.socket.js' ) );
var Telegram = require( path.join( global.dir, 'lib/fancy.telegram.js' ) );

/**
  *  Klasa ładująca moduły
  *
  *  @author     Orkin (AVNTeam.net)
  */
class Modules {

/**
  *  Konstruktor klasy
  *
  *  @author     Orkin (AVNTeam.net)
  */
  constructor( config ) {
    this._log = logger.getLogger( 'MODULES' );
    this._log.setLevel( global.loglevel );
    // Ładowanie języka
    this._lang = new Lang( global.locale );
    // Otwieranie socketów
    this._socket = ( config.socket.enabled ? new Socket( config.socket ) : null );
    // Podpinanie się pod telegrama
    this._telegram = ( config.telegram.enabled ? new Telegram( config.telegram ) : null );

    //Ładowanie modułów
    this._modules = this.loadModules();
    return this._modules;
  }

/**
  *  Metoda ładująca moduły
  *
  *  @author     Orkin (AVNTeam.net)
  */
  loadModules() {
    var self = this;
    var count = 0;
    // Wczytywanie modułów z folderu modules oprócz disabled-modules
    // zagłębiaj się w foldery i podłączaj tylko pliki o nazwie main.js
    var settings = {
      dirname: path.join( global.dir, 'modules' ),
      filter: /main\.js$/,
      excludeDirs: /^disabled-modules$/,
      recursive: true,
      resolve: function( Module ) {
        count++;
        return new Module( null, Config, Lang, logger, self._socket, self._telegram );
      }
    }
    var modules = requireAll( settings );
    this._log.info( this._lang.get( 'modules.loaded', count ) );
    return modules;
  }

}

module.exports = Modules;
