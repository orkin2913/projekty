'use strict';

var fs = require( 'fs' );
var path = require( 'path' );
var logger = require( 'log4js' );

/**
  *  Klasa do zarzadzania configiem
  *
  *  @author     Orkin (AVNTeam.net)
  */
class Lang {

/**
  *  Konstruktor klasy
  *
  *  @author     Orkin (AVNTeam.net)
  */
  constructor( locale = 'en', localPath ) {
    this._log = logger.getLogger( 'lang' );
    this._log.setLevel( global.loglevel );
    this._locale = locale;
    this._file = 'lang.' + this._locale;
    this._localPath = localPath || null;
    this._path = path.join( this._localPath || global.dir, 'locale', this._file + '.json' );
    this._loadFile();
  }

/**
  *  Funkcja zwracająca aktualny język
  *
  *  @author     Orkin (AVNTeam.net)
  */
  getLocale() {
    return this._locale;
  }

/**
  *  Funkcja zmieniająca aktualny język
  *
  *  @author     Orkin (AVNTeam.net)
  */
  setLocale( locale ) {
    this._locale = locale;
    this._file = 'lang.' + this._locale;
    this._path = path.join( this._localPath || global.dir, 'locale', this._file + '.json' );
    if ( !this._localPath ) global.locale = locale;
    this._loadFile();
  }

/**
  *  Funkcja zwracająca dane z lokalizacji
  *
  *  @author     Orkin (AVNTeam.net)
  */
  get( element ) {
    // Znajdź wartość w pliku z lokalizacją (poruszaj się bo obiektach)
    var ret = this._findValue( element );
    // Jeżeli w stringu są znaki zapytania i zmienne w argumentach, to zamień
    // zwróć zamienione
    return this._bind( ret, arguments );
  }

/**
  *  Funkcja szukająca w lokalizacji odpowiedniej wartości
  *
  *  @author     Orkin (AVNTeam.net)
  */
  _findValue( element ) {
    element = element.split( '.' );
    var ret = this._data;
    // Na podstawie tablicy elementów
    // znajdź w obiektach lokalizacji odpowiednią wartość
    element.map( ( key, index ) => {
      ret = ret[ key ];
    } );
    return ret;
  }

/**
  *  Funkcja zamieniająca znaki zapytania na wartości podane w argumentach
  *
  *  @author     Orkin (AVNTeam.net)
  */
  _bind( ret, args ) {
    delete args[ 0 ];
    var i = 1;
    if ( args ) {
      ret = ret.replace(/\$\{\?\}/g, () => {
        return args[ i++ ];
      } );
    }
    return ret;
  }

/**
  *  Funkcja ładująca plik
  *
  *  @author     Orkin (AVNTeam.net)
  */
  _loadFile() {
    try {
      this._data = require( this._path );
      return true;
    } catch ( error ) {
      // this._log.error( error.message );
      this._log.error( 'Unable to load the selected language (' + this.getLocale() + '). Trying to load English' );
      if ( this.getLocale() !== 'en' ) {
        this.setLocale( 'en' );
      } else {
        this._log.error( 'Cannot load any lang file' );
        process.exit();
      }
      return 0;
    }
  }

}

module.exports = Lang;
