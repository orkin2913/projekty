'use strict';

var fs = require( 'fs' );
var path = require( 'path' );
var YAML = require( 'yamljs' );
var logger = require( 'log4js' );
var Lang = require( path.join( global.dir, 'lib/fancy.lang.js' ) );

/**
  *  Klasa do zarzadzania configiem
  *
  *  @author     Orkin (AVNTeam.net)
  */
class Config {

/**
  *  Konstruktor klasy
  *
  *  @author     Orkin (AVNTeam.net)
  */
  constructor( file, localPath ) {
    this._log = logger.getLogger( 'CONFIG' );
    this._log.setLevel( global.loglevel );
    this._file = file || 'config';
    this._path = path.join( localPath || global.dir, 'cfg', this._file + '.yml' );
    this._data = {};

    // Ładowanie języka
    this._lang = new Lang( global.locale );
  }

/**
  *  Funkcja pobierająca dane z pliku
  *
  *  @author     Orkin (AVNTeam.net)
  */
  getConfig() {
    try {
      // Wczytaj i zrób parse z Yamla
      this._data = YAML.load( this._path );
      this._log.debug( this._lang.get( 'config.loaded' ) + " " + this._path );
      return this._data;

    } catch ( error ) {
      this._log.error( error.message );
      process.exit();
    }
  }

/**
  *  Funkcja zapisująca dane do pliku
  *
  *  @author     Orkin (AVNTeam.net)
  */
  setConfig( data ) {
    if ( data && typeof data === 'object' ) this._data = data;
    // Zamień obiekt na YAML
    // Do 10 "głębokości" wszystko do nowej linii, później w jednej linii
    // Używaj dwóch spacji do wcięć
    var dataString = YAML.stringify( this._data, 10, 2 );
    fs.writeFile( this._path, dataString, ( error ) => {
      if ( error ) {
        this._log.error( error );
        return false;
      } else {
        return true;
      }
    } );
  }

}

module.exports = Config;
