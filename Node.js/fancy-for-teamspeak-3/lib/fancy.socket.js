'use strict';

var fs = require( 'fs' );
var net = require('net');
var path = require( 'path' );
var logger = require( 'log4js' );
var express = require( 'express' );
var publicIp = require( 'public-ip' );
var EventEmitter = require( 'events' );
var bodyParser = require( 'body-parser' );
var Lang = require( path.join( global.dir, 'lib/fancy.lang.js' ) );


/**
  *  Klasa otwierająca socket
  *
  *  @author     Orkin (AVNTeam.net)
  */
class Socket extends EventEmitter {

/**
  *  Konstruktor klasy
  *
  *  @author     Orkin (AVNTeam.net)
  */
  constructor( config ) {
    // http://stackoverflow.com/questions/34947427/how-to-resolve-this-is-not-defined-when-extending-eventemitter
    super();
    var port = config.port;

    this._log = logger.getLogger( 'SOCKET' );
    this._log.setLevel( global.loglevel );
    // Ładowanie języka
    this._lang = new Lang( global.locale );

    // Pobieranie publicznego IP
    publicIp.v4( { 'timeout': 300 } )
      .then( ( ip ) => {
      	this._ip = ip;
        this._tcpServer( port );
        this._webServer( port + 1 );
        this.emit( 'loaded' );
      } )
      .catch( ( err ) => {
        this._log.error( this._lang.get( 'socket.noconnection' ) );
        this._ip = null;
        this._tcpServer( port );
        this._webServer( port + 1 );
        this.emit( 'loaded' );
      } );
  }

/**
  *  Funkcja otwierająca serwer TCP
  *
  *  @author     Orkin (AVNTeam.net)
  */
  _tcpServer( port ) {
    net.createServer( ( socket ) => this._tcpSocket( socket ) ).listen( port );
    this._log.info( this._lang.get( 'socket.listening' ) + ' TCP ' + ( this._ip || 'localhost' ) + ':' + port );
  }

/**
  *  Funkcja obsługująca clientów
  *
  *  @author     Orkin (AVNTeam.net)
  */
  _tcpSocket( socket ) {
    // Nazwij klienta
    socket.name = socket.remoteAddress + ':' + socket.remotePort;

    // Połączono
    this.emit( 'connect', socket.name );
    // Wysłano dane
    socket.on('data', ( data ) => {
      data = data.toString();
      this.emit( 'data', socket.name, data );
    });
    // Rozłączono
    socket.on('end', () => {
      this.emit( 'disconnect', socket.name );
    });
  }

/**
  *  Funkcja otwierająca serwer HTTP
  *
  *  @author     Orkin (AVNTeam.net)
  */
  _webServer( port ) {
    this.webServer = express();
    this.webServer.use( bodyParser.urlencoded( { extended: false } ) );
    this.webServer.use( bodyParser.json() );
    // this._webServerGet();
    this._webServerPost();
    this.webServer.listen( port );
    this._log.info( this._lang.get( 'socket.listening' ) + ' HTTP http://' + ( this._ip || 'localhost' ) + ':' + port );
  }

/**
  *  Funkcja obsługująca zapytania GET
  *
  *  @author     Orkin (AVNTeam.net)
  */
  _webServerGet() {
    this.webServer.get( '/', ( req, res ) => {
      var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
      var html = '<form method="POST" action=""><input type="text" name="test"><input type="submit"></form>';
      res.writeHead( 200, { 'Content-Type': 'text/html' } );
      res.end( html );
    });
  }

/**
  *  Funkcja obsługująca zapytania POST
  *
  *  @author     Orkin (AVNTeam.net)
  */
  _webServerPost() {
    this.webServer.post( '/', ( req, res ) => {
      var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
      this.emit( 'postData', ip, req.body );
      res.writeHead( 200, { 'Content-Type': 'text/html' } );
      res.end( '1' );
    });
  }

}

module.exports = Socket;
