'use strict';

var path = require( 'path' );
var argv = require( 'yargs-parser' ) ( process.argv.slice( 2 ) );
var logger = require( 'log4js' );

// Ustawianie wartości globalnych
// Ustawianie ścieżki do katalogu "domowego"
global.dir = __dirname.substring(0, __dirname.length - 3);
global.locale = 'en';
global.loglevel = 'info';

// Inicjowanie loggera
var log = logger.getLogger( 'MAIN' );
log.setLevel( global.loglevel );

var Lang = require( path.join( global.dir, 'lib/fancy.lang.js' ) );
var Config = require( path.join( global.dir, 'lib/fancy.config.js' ) );
var Modules = require( path.join( global.dir, 'lib/fancy.modules.js' ) );

// Ładowanie języka
var lang = new Lang( global.locale );

// Ładowanie konfiguracji
// Bierzemy pod uwagę parametry "-c" oraz "--config" przekazujące nazwę konfiguracji
// ułatwiające uruchamianie bota na hostingach
log.info( lang.get( 'config.loading' ) );
var config = new Config( argv.config || argv.c || 'config' );
var configData = config.getConfig();

log.setLevel( global.loglevel );
lang.setLocale( configData.app.locale );

// Aktualizowanie wartości globalnych na podstawie konfiguracji
global.loglevel = configData.app.loglevel;

log.info( lang.get( 'modules.loading' ) );
var modules = new Modules( configData );
