<?php
ini_set('display_errors',"1");
if (isset($_POST['cldbid'])) {
$cldbid = $_POST['cldbid'];
$cldbid = intval($cldbid);
require_once("../lib/ts3admin/ts3admin.class.php");
require("../config.php");

$mysqli = new mysqli($db['host'], $db['user'], $db['password'], $db['database']);

if($mysqli->connect_error) {
    die('Error : ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
}
$results = $mysqli->query("SELECT `key`, `value` FROM `avnbot_config` WHERE `key`='ip' OR `key`='query_port' OR `key`='port' OR `key`='login' OR `key`='password' OR `key`='nick' OR `key`='lang'");

while($row = $results->fetch_array()) {
   $config[$row["key"]] = $row["value"];
}

$results->free();

$lang = file_get_contents("../lang/lang.".$config['lang'].".json");
$lang = json_decode($lang, true);

$tsAdmin = new ts3admin($config['ip'], $config['query_port']);
if($tsAdmin->getElement('success', $tsAdmin->connect())) {
	$tsAdmin->login($config['login'], $config['password']);
	$tsAdmin->selectServer($config['port']);
	$tsAdmin->setName($config['nick'].' Login');
	$clients = $tsAdmin->clientList();
	foreach ($clients['data'] as $client) {
		if ($client['client_database_id'] == $cldbid) {
			$results = $mysqli->query("SELECT * FROM `avnbot_login_codes` WHERE `cldbid`= ".$cldbid."");
			if (mysqli_num_rows($results) > 0) {
				$results = $results->fetch_array(MYSQLI_ASSOC);
				if ($results['sent'] == 0) {
					$tsAdmin->sendMessage(1, $client['clid'], $lang[790].' '.$results['code'].'');
					$tsAdmin->sendMessage(1, $client['clid'], $lang[791]);
					$mysqli->query("UPDATE `avnbot_login_codes` SET `sent` = 1 WHERE `cldbid`= ".$cldbid."");
				}
			}
			break;
		}
	}
	
	$tsAdmin->logout();
	$tsAdmin = null;
	$mysqli->close();
	echo 1;
} else {
	echo 0;
}
} else {
	echo 0;
}
?>