<?php
class tools {
	//	Zmienna przechowująca dane logowania do bazy danych
	private $db;
	
	//	Konstruktor
	//	@ param (1)
	public function __construct($database) {
		$this->db = $database;
		
		// Prawdziwe IP poprzez cloudflare
		if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
			$_SERVER['REMOTE_ADDR'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
		}
		$this->session();
	}
	
	//	Funkcja zwracająca tablicę z konfiguracją panelu
	//	@ return (1)
	public function loadConfig() {
		$mysqli = new mysqli($this->db['host'], $this->db['user'], $this->db['password'], $this->db['database']);
		if ($mysqli->connect_error) {
			die('Error: ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
		}
		$mysqli->set_charset("utf8");
		$results = $mysqli->query("SELECT `key`, `value` FROM `avnbot_portal`");

		//Budowanie tablicy z konfiguracją
		while ($row = $results->fetch_array()) {
		   $config[$row["key"]] = $row["value"];
		}

		$results->free();
		$mysqli->close();
		return $config;
	}
	
	//	Funkcja zwracająca tablicę z konfiguracją bota
	//	@ return (1)
	public function loadBotConfig() {
		$mysqli = new mysqli($this->db['host'], $this->db['user'], $this->db['password'], $this->db['database']);
		if ($mysqli->connect_error) {
			die('Error: ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
		}
		$mysqli->set_charset("utf8");
		$results = $mysqli->query("SELECT `key`, `value` FROM `avnbot_config`");

		//Budowanie tablicy z konfiguracją
		while ($row = $results->fetch_array()) {
		   $config[$row["key"]] = $row["value"];
		}

		$results->free();
		$mysqli->close();
		return $config;
	}
	
	//	Funkcja generuje kod logowania dla clienta
	//	@ param (1) | return (1)
	public function genCode($cldbid) {
		$cldbid = intval($cldbid);
		$mysqli = new mysqli($this->db['host'], $this->db['user'], $this->db['password'], $this->db['database']);
		if ($mysqli->connect_error) {
			die('Error: ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
		}
		$mysqli->set_charset("utf8");
		$date = Date('l jS F Y h:i:s A'); $date = strtotime($date);
		$deldate = $date - 300;
		
		$seed = str_split('ABCDEFGHIJKLMNOPQRSTUVWXYZ'
                 .'0123456789'); // and any other characters
		shuffle($seed); // probably optional since array_is randomized; this may be redundant
		$rand = '';
		foreach (array_rand($seed, 6) as $k) $rand .= $seed[$k];
		$date = Date('l jS F Y h:i:s A'); $date = strtotime($date);
		
		$mysqli->query("DELETE FROM `avnbot_login_codes` WHERE `created`< ".$deldate);
		$results = $mysqli->query("SELECT * FROM `avnbot_login_codes` WHERE `cldbid`= ".$cldbid);
		if (mysqli_num_rows($results) > 0) {
			$results = $results->fetch_array(MYSQLI_ASSOC);
			if ($results['created'] < $deldate) {
				$mysqli->query("DELETE FROM `avnbot_login_codes` WHERE `cldbid`= ".$cldbid);
				$mysqli->query("INSERT INTO `avnbot_login_codes` (`cldbid`, `code`, `created`, `sent`) VALUES (".$cldbid.", '".$rand."', ".$date.", 0)");
				return true;
			} else {
				return false;
			}
		} else {
			$mysqli->query("INSERT INTO `avnbot_login_codes` (`cldbid`, `code`, `created`, `sent`) VALUES (".$cldbid.", '".$rand."', ".$date.", 0)");
			return true;
		}
		$mysqli->close();
	}
	
	//	Funkcja sprawdza kod logowania
	//	@ param (2) | return (1)
	public function checkCode($code, $cldbid) {
		$cldbid = intval($cldbid);
		$code = addslashes($code);
		$mysqli = new mysqli($this->db['host'], $this->db['user'], $this->db['password'], $this->db['database']);
		if ($mysqli->connect_error) {
			die('Error: ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
		}
		$mysqli->set_charset("utf8");
		$date = Date('l jS F Y h:i:s A'); $date = strtotime($date);
		$deldate = $date - 300;
		
		$results = $mysqli->query("SELECT * FROM `avnbot_login_codes` WHERE `cldbid`= ".$cldbid);
		if (mysqli_num_rows($results) > 0) {
			$results = $results->fetch_array(MYSQLI_ASSOC);
			if ($results['created'] > $deldate) {
				if ($results['code'] == $code) {
					$mysqli->query("DELETE FROM `avnbot_login_codes` WHERE `cldbid`= ".$cldbid);
					return true;
				} else return false;
			} else {
				$mysqli->query("DELETE FROM `avnbot_login_codes` WHERE `cldbid`= ".$cldbid);
				return false;
			}
		} else {
			return false;
		}
		$mysqli->close();
	}
	
	//	Funkcja zwracająca dane wszystkich clientów
	//	@ return (1)
	public function loadAllClients() {
		$config = $this->loadConfig();
		$lang = $this->loadLang($config['lang']);
		$mysqli = new mysqli($this->db['host'], $this->db['user'], $this->db['password'], $this->db['database']);
		if ($mysqli->connect_error) {
			die('Error: ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
		}
		$mysqli->set_charset("utf8");
		if (!empty($config['ranking_exceptions']))
			$results = $mysqli->query("SELECT `cldbid`, `nick`, `created`, `cluid`, `totalconnections`, `pkt`, `time_online`, `time_afk`, `record` FROM `avnbot_clients` WHERE `cldbid` NOT IN (".$config['ranking_exceptions'].")");
		else
			$results = $mysqli->query("SELECT `cldbid`, `nick`, `created`, `cluid`, `totalconnections`, `pkt`, `time_online`, `time_afk`, `record` FROM `avnbot_clients`");
		$tabelka = '';
		while($row = $results->fetch_array()) {
			//Czas online, czas AFK, rekord
			$client['time_online'] = $this->secToTime($row['time_online']); $client['time_online'] = $client['time_online']['dni'].'d '.$client['time_online']['godziny'].'h '.$client['time_online']['minuty'].'m '.$client['time_online']['sekundy'].'s';
			$client['time_afk'] = $this->secToTime($row['time_afk']); $client['time_afk'] = $client['time_afk']['dni'].'d '.$client['time_afk']['godziny'].'h '.$client['time_afk']['minuty'].'m '.$client['time_afk']['sekundy'].'s';
			$client['record'] = $this->secToTime($row['record']); $client['record'] = $client['record']['dni'].'d '.$client['record']['godziny'].'h '.$client['record']['minuty'].'m '.$client['record']['sekundy'].'s';
			$tabelka .= '						<tr>
							<td></td>
							<td>'.$row['cldbid'].'</td>
							<td><a href="profile.php?id='.$row['cldbid'].'">'.$row['nick'].'</a></td>
							<td><span class="hidden">'.date("Y/m/d", $row['created']).'</span>'.date($lang[59], $row['created']).'</td>
							<td>'.$row['totalconnections'].'</td>
							<td>'.$row['pkt'].'</td>
							<td><span class="hidden">'.$row['time_online'].'</span></td>
							<td>'.$client['time_online'].'</td>
							<td><span class="hidden">'.$row['time_afk'].'</span></td>
							<td>'.$client['time_afk'].'</td>
							<td><span class="hidden">'.$row['record'].'</span></td>
							<td>'.$client['record'].'</td>
							<td>'.$row['cluid'].'</td>
						</tr>';
			$client = null;
		}
		$results->free();
		$mysqli->close();
		return $tabelka;
	}
	
	//	Funkcja zwracająca dane clienta z bazy
	//	@ param (1) | return (1)
	public function loadClient($cldbid) {
		$config = $this->loadConfig();
		function ranking($col) {
			global $config;
			if (!empty($config['ranking_exceptions']))
				return 'FIND_IN_SET( `'.$col.'`, ( SELECT GROUP_CONCAT( `'.$col.'` ORDER BY `'.$col.'` DESC ) FROM `avnbot_clients` WHERE `cldbid` NOT IN ('.$config['ranking_exceptions'].') ) ) AS `'.$col.'_rank`';
			else
				return 'FIND_IN_SET( `'.$col.'`, ( SELECT GROUP_CONCAT( `'.$col.'` ORDER BY `'.$col.'` DESC ) FROM `avnbot_clients`) ) AS `'.$col.'_rank`';
		}
		
		$mysqli = new mysqli($this->db['host'], $this->db['user'], $this->db['password'], $this->db['database']);
		if ($mysqli->connect_error) {
			die('Error: ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
		}
		$mysqli->set_charset("utf8");
		if ($SQL = $mysqli->prepare('SELECT `id`, `cldbid`, `nick`, `created`, `cluid`, `totalconnections`, '.ranking('totalconnections').', `ip`, `avatar`, `pkt`, '.ranking('pkt').', `time_online`, '.ranking('time_online').', `time_afk`, '.ranking('time_afk').', `record`, '.ranking('record').', `status`, `status_desc`, `groups`, `last_seen` FROM `avnbot_clients` WHERE `cldbid` = ?')) {
			//Dla wielu http://stackoverflow.com/a/16612474
			$SQL->bind_param("s", $cldbid);
			$SQL->execute();
			$results = $SQL->get_result();
			$client = $results->fetch_assoc();
		}

		$results->free();
		$mysqli->close();
		return $client;
	}
	
	//	Funkcja zwracająca ilość użytkowników w bazie
	//	@ return (1)
	public function clientsCount() {
		$mysqli = new mysqli($this->db['host'], $this->db['user'], $this->db['password'], $this->db['database']);
		if ($mysqli->connect_error) {
			die('Error: ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
		}
		$mysqli->set_charset("utf8");
		$results = $mysqli->query("SELECT `id` FROM `avnbot_clients`");
		$ret = $results->num_rows;
		$results->free();
		$mysqli->close();
		return $ret;
	}
	
	//	Funkcja zwracająca kanał użytkownika
	//	@ return (1)
	public function clientChannel($cldbid) {
		$config = $this->loadBotConfig();
		$mysqli = new mysqli($this->db['host'], $this->db['user'], $this->db['password'], $this->db['database']);
		if ($mysqli->connect_error) {
			die('Error: ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
		}
		$mysqli->set_charset("utf8");
		$results = $mysqli->query("SELECT * FROM `avnbot_channels`");
		
		$ret = false;
		
		while ($row = $results->fetch_assoc()) {
			$row['owner'] = explode(',', $row['owner']);
			if (in_array($cldbid, $row['owner'])) {
				$ret = $row;
				$channels = $this->loadCache('channelList');
				foreach ($channels as $channel) {
					if ($channel['cid'] == $ret['cid'])
						$ret['channel_name'] = '<a href="ts3server://'.$config['ip'].':'.$config['port'].'?cid='.$channel['cid'].'">'.$channel['channel_name'].'</a>';
				}
				break;
			}
		}
		
		$results->free();
		$mysqli->close();
		return $ret;
	}
	
	//	Funkcja zwracająca listę banów
	//	@ return (1)
	public function loadBans() {
		$config = $this->loadConfig();
		$lang = $this->loadLang($config['lang']);
		$bans = $this->loadCache("banList");
		
		$today = date('Y-m-d');
		$tomorrow = date('Y-m-d', strtotime('tomorrow')); 
		$yesterday = date('Y-m-d', strtotime('yesterday')); 
		
		$tabelka = '';
		$length = count($bans);
		
		//Otwieranie połączenia z bazą danych
		$mysqli = new mysqli($this->db['host'], $this->db['user'], $this->db['password'], $this->db['database']);
		if ($mysqli->connect_error) {
			die('Error: ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
		}
		$mysqli->set_charset("utf8");
		
		if (!empty($bans)) {
			for ($i = 0; $i < $length; $i++) {
				$ban = $bans[$i];
				if ($i+1 < $length)
					$next = $bans[$i+1];
				
				//Kiedy wygasa
				if ($ban['duration'] == 0) $ban['duration'] = $lang[506];
				else {
					$ban['duration'] = $ban['created'] + $ban['duration'];
					$date = date('Y-m-d', $ban['duration']);
					if ($date == $today) $ban['duration'] = $lang[507].' '.date('G:i', $ban['duration']);
					elseif ($date == $tomorrow) $ban['duration'] = $lang[509].' '.date('G:i', $ban['duration']);
					else $ban['duration'] = date($lang[570], $ban['duration']);
				}
				
				//Opis bana
				if (($next['reason'] == $ban['reason']) && ($next['created'] = $ban['created']) && ($next['duration'] = $ban['duration']) && ($next['invokeruid'] = $ban['invokeruid'])) {
					if (($next['name'] != '') && ($ban['name'] == '')) $ban['name'] = $next['name'];
					if ((!empty($next['ip'])) && (empty($ban['ip']))) $ban['ip'] = $next['ip'];
					if ((!empty($next['uid'])) && (empty($ban['uid']))) $ban['uid'] = $next['uid'];
					$i++;
				}
				
				//Cenzura na IP
				if (!empty($ban['ip'])) {
					$ban['ip'] = str_replace("\\", "", $ban['ip']);
					$ban['ip'] = explode('.', $ban['ip']);
					$ban['ip'] = $ban['ip'][0].'.*.*.*';
				}
				
				//Czas utworzenia
				$date = date('Y-m-d', $ban['created']);
				if ($date == $today) $ban['created'] = $lang[507].' '.date('G:i', $ban['created']);
				elseif ($date == $yesterday) $ban['created'] = $lang[508].' '.date('G:i', $ban['created']);
				else $ban['created'] = date($lang[570], $ban['created']);
				
				//Z bazy info o profilu
				if (!empty($ban['uid'])) {
					if ($results = $mysqli->query('SELECT `cldbid`, `nick` FROM `avnbot_clients` WHERE `cluid` = \''.$ban['uid'].'\'')) {
						if (mysqli_num_rows($results) > 0) {
							$client = $results->fetch_assoc();
							if (empty($ban['name'])) $ban['name'] = '<a href="profile.php?id='.$client['cldbid'].'">'.$client['nick'].'</a>';
							else $ban['name'] = '<a href="profile.php?id='.$client['cldbid'].'">'.$ban['name'].'</a>';
							$results->free();
						}
					}
				}
				
				$tabelka .= '						<tr>
									<td>'.$ban['banid'].'</td>
									<td>'.$ban['name'].'</td>
									<td>'.$ban['ip'].'</td>
									<td>'.$ban['uid'].'</td>
									<td>'.$ban['reason'].'</td>
									<td><a href="profile.php?id='.$ban['invokercldbid'].'">'.$ban['invokername'].'</a></td>
									<td>'.$ban['created'].'</td>
									<td>'.$ban['duration'].'</td>
								</tr>';
			}
		}
		$mysqli->close();
		return $tabelka;
	}
	
	//	Funkcja nadająca status zbanowany (jakby co)
	//	@ param (2)
	public function fixBan($cluid, $reason) {
		$mysqli = new mysqli($this->db['host'], $this->db['user'], $this->db['password'], $this->db['database']);
		if ($mysqli->connect_error) {
			die('Error: ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
		}
		$mysqli->set_charset("utf8");
		$results = $mysqli->query("UPDATE `avnbot_clients` SET `status`=3, `status_desc`='".$reason."' WHERE `cluid`='".$cluid."'");
		$mysqli->close();
	}
	
	//	Funkcja zwracająca odpowiednią lokalizację
	//	@ param (1) | return (1)
	public function loadLang($lang) {
		$obj = file_get_contents("lang/lang.$lang.json");
		return json_decode($obj, true);
	}
	
	//	Funkcja zwracająca zawartość wybranego pliku z cache
	//	@ param (1) | return (1)
	public function loadCache($file) {
		$config = $this->loadConfig();
		if (!file_exists("cache/$file.json")) require("lib/cron/cache.php");
		elseif (time() - filemtime("cache/$file.json") >= $config['update_cache']) require("lib/cron/cache.php");
		$obj = file_get_contents("cache/$file.json");
		return json_decode($obj, true);
	}
	
	//	Funkcja zwracająca status administratorów
	//	@ return (1)
	public function adminStatus() {
		$config = $this->loadBotConfig();
		$serverGroupList = $this->loadCache('serverGroupList');
		$clientList = $this->loadCache('clientList');
		$adminGroups = explode(',', $config['admins']);
		$adminStatus = '';
		foreach ($serverGroupList as $group) {
			if (in_array($group['sgid'], $adminGroups)) {
				$adminStatus .= '<div class="col-md-12 col-xs-12 text-center"><h4>'.$group['name'].'</h4></div>';
				$clients = $this->loadGroupClients($group['sgid']);
				foreach ($clients as $client) {
					if ($client['cldbid'] != 1) {
						$statusek = '<span class="label label-danger">offline</span></div>';
						$adminStatus .= '<div class="col-md-8 col-xs-8"><a href="profile.php?id='.$client['cldbid'].'">'.$client['client_nickname'].'</a></div><div class="col-md-4 col-xs-4 text-right">';
						foreach ($clientList as $cl) {
							if ($cl['client_database_id'] == $client['cldbid']) {
								$statusek= '<span class="label label-success">online</span></div>';
								break;
							}
						}
						$adminStatus .= $statusek;
					}
				}
			}
		}
		return $adminStatus;
	}
	
	//	Funkcja zwracająca listę adminów
	//	@ return (1)
	public function adminList() {
		$config = $this->loadBotConfig();
		$serverGroupList = $this->loadCache('serverGroupList');
		$clientList = $this->loadCache('clientList');
		$channelList = $this->loadCache('channelList');
		$adminGroups = explode(',', $config['admins']);
		$adminStatus = '';
		foreach ($serverGroupList as $group) {
			if (in_array($group['sgid'], $adminGroups)) {
				$adminStatus .= '<div class="col-md-12"><h4>'.$group['name'].'</h4></div>';
				$adminStatus .= '<div class="panel panel-default"><div class="table-responsive" style="overflow-x:initial;"><table class="table table-striped">
					<thead>
						<tr>
							<th><div class="col-md-12">Nick</div></th>
							<th><div class="col-md-12">Kanał</div></th>
							<th><div class="col-md-12 text-right">Status</div></th>
						</tr>
					</thead>';
				$clients = $this->loadGroupClients($group['sgid']);
				foreach ($clients as $client) {
					if ($client['cldbid'] != 1) {
						
						$adminStatus .= '<tr>';
						$adminStatus .= '<td><div class="col-md-12"><a href="profile.php?id='.$client['cldbid'].'">'.$client['client_nickname'].'</a></div></td>';
						$kanal = '<td><div class="col-md-12"></div></td>';
						$statusek = '<td><div class="col-md-12 text-right"><span class="label label-danger">offline</span></div></td>';
						foreach ($clientList as $cl) {
							if ($cl['client_database_id'] == $client['cldbid']) {
								foreach ($channelList as $channel) {
									if ($channel['cid'] == $cl['cid']) {
										$kanal = '<td><div class="col-md-12"><a href="ts3server://'.$config['ip'].':'.$config['port'].'?cid='.$channel['cid'].'">'.$channel['channel_name'].'</a></div></td>';
										break;
									}
								}
								$statusek= '<td><div class="col-md-12 text-right"><span class="label label-success">online</span></div></td>';
								break;
							}
						}
						
						$adminStatus .= $kanal;
						$adminStatus .= $statusek;
						
						$adminStatus .= '</tr>';
					}
				}
				$adminStatus .= '</table></div></div>';
			}
		}
		return $adminStatus;
	}
	
	//	Funkcja zwracająca listę clientów w danej grupie
	//	@ param (1) | return (1)
	public function loadGroupClients($sgid) {
		$config = $this->loadConfig();
		$botConfig = $this->loadBotConfig();
		if ((!file_exists("cache/groups/group_$sgid.json")) || (time() - filemtime("cache/groups/group_$sgid.json") >= $config['update_cache'])) {
			require_once("lib/ts3admin/ts3admin.class.php");
			$tsAdmin = new ts3admin($botConfig['ip'], $botConfig['query_port']);
			if($tsAdmin->getElement('success', $tsAdmin->connect())) {
				$tsAdmin->login($botConfig['login'], $botConfig['password']);
				$tsAdmin->selectServer($botConfig['port']);
				$tsAdmin->setName($botConfig['nick'].' Cache');
				$cache = $tsAdmin->getElement('data', $tsAdmin->serverGroupClientList($sgid, true));
				$hand = fopen("cache/groups/group_$sgid.json", "w+");
				fwrite($hand, json_encode($cache));
				fclose($hand);
				$tsAdmin->logout();
			} else {
				touch("cache/groups/group_$sgid.json");
			}
			$tsAdmin = null;
		}
		$obj = file_get_contents("cache/groups/group_$sgid.json");
		return json_decode($obj, true);
	}
	
	//	Funkcja zwracająca drzewo kanałów
	//	@ param (1) | return (1)
	public function loadTree($channels = null, $cachefile = "AllChannels", $hideParent = false) {
		$config = $this->loadBotConfig();
		require_once("lib/tsstatus/tsstatus.php");
		$tsstatus = new TSStatus($config['ip'], $config['query_port']);
		$tsstatus->useServerPort($config['port']);
		$tsstatus->imagePath = "img/icons/";
		$tsstatus->timeout = 2;
		$tsstatus->setLoginPassword($config['login'], $config['password']);
		if ($channels) {
			$ids = explode(",", $channels);
			call_user_func_array(array($tsstatus, "limitToChannels"), $ids);
		}
		$tsstatus->setCache(60, "cache/channels/".$cachefile.".cache");
		$tsstatus->hideParentChannels = $hideParent;
		return $tsstatus->render();
	}
	
	//	Funkcja rysująca górne menu
	//	@ param (1) | return (1)
	public function drawMenu() {
		$config = $this->loadConfig();
		$lang = $this->loadLang($config['lang']);
		if (!isset($_SESSION['cldbid'])) {
			$top = '<li><a href="login.php">'.$lang['m07'].'</a></li>';
		} else {
			$top = '<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><img src="'.$_SESSION['avatar'].'" class="img-circle" width="20px" height="20px"> '.$_SESSION['nick'].' <i class="fa fa-caret-down"></i></a>
									<ul class="dropdown-menu">
										<li><a href="profile.php?id='.$_SESSION['cldbid'].'"><i class="fa fa-user"></i>&nbsp;&nbsp;'.$lang['m08'].'</a></li>
										<li><a href="#"><i class="fa fa-universal-access"></i>&nbsp;&nbsp;'.$lang['m09'].'</a></li>
										<li><a href="#"><i class="fa fa-folder-open"></i>&nbsp;&nbsp;'.$lang['m10'].'</a></li>
										<li><a href="#"><i class="fa fa-shield"></i>&nbsp;&nbsp;'.$lang['m11'].'</a></li>
										<li><a href="#"><i class="fa fa-hashtag"></i>&nbsp;&nbsp;'.$lang['m12'].'</a></li>
										<li role="separator" class="divider"></li>
										<li><a href="logout.php"><i class="fa fa-sign-out"></i>&nbsp;&nbsp;'.$lang['m13'].'</a></li>
									</ul>
								</li>';
		}
		return $top;
	}
	
	//	Funkcja zamieniająca sekundy na czas
	//	@ param (1) | return (1)
	public function secToTime($param) {
		$time['dni'] = floor($param/86400);
		$param -= $time['dni']*86400;
		$time['godziny'] = floor($param/3600);
		$param -= $time['godziny']*3600;
		$time['minuty'] = floor($param/60);
		$param -= $time['minuty']*60;
		$time['sekundy'] = $param;
		return $time;
	}
	
	//	Zarządzanie sesją
	private function session() {
		session_start();
		if (!isset($_SESSION['initiated']))
		{
			session_regenerate_id();
			$_SESSION['initiated'] = true;
		}
	}
}
?>