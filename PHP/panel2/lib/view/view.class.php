<?php
	class View {
	
		private $vars;
		
		public function __get($var) {
			if (isset($this->vars [$var])) {
				return $this->vars[$var];
			}
		}
		
		public function assign($var, $value) {
			$this->vars [$var] = $value;
		}
		
		public function show($template) {
			ob_start();
			require sprintf("templates/%s", 'top.tpl');
			require sprintf("templates/%s", $template);
			require sprintf("templates/%s", 'bottom.tpl');
			return ob_get_flush();
		}
	}
?>