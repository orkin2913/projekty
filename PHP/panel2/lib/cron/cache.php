<?php
require_once("lib/ts3admin/ts3admin.class.php");
require("config.php");

$mysqli = new mysqli($db['host'], $db['user'], $db['password'], $db['database']);

if($mysqli->connect_error) {
    die('Error : ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
}
$results = $mysqli->query("SELECT `key`, `value` FROM `avnbot_config` WHERE `key`='ip' OR `key`='query_port' OR `key`='port' OR `key`='login' OR `key`='password' OR `key`='nick'");

while($row = $results->fetch_array()) {
   $config[$row["key"]] = $row["value"];
}

$results->free();
$mysqli->close();

$tsAdmin = new ts3admin($config['ip'], $config['query_port']);
if($tsAdmin->getElement('success', $tsAdmin->connect())) {
	$tsAdmin->login($config['login'], $config['password']);
	$tsAdmin->selectServer($config['port']);
	$tsAdmin->setName($config['nick'].' Cache');
	$cache = $tsAdmin->getElement('data', $tsAdmin->serverInfo());
	$hand = fopen('cache/serverInfo.json', "w+");
	fwrite($hand, json_encode($cache));
	fclose($hand);
	$cache = $tsAdmin->getElement('data', $tsAdmin->serverGroupList());
	$hand = fopen('cache/serverGroupList.json', "w+");
	fwrite($hand, json_encode($cache));
	fclose($hand);
	$cache = $tsAdmin->getElement('data', $tsAdmin->channelGroupList());
	$hand = fopen('cache/channelGroupList.json', "w+");
	fwrite($hand, json_encode($cache));
	fclose($hand);
	$cache = $tsAdmin->getElement('data', $tsAdmin->channelList("-topic -flags -voice -limits -icon -seconds_empty"));
	$hand = fopen('cache/channelList.json', "w+");
	fwrite($hand, json_encode($cache));
	fclose($hand);
	$cache = $tsAdmin->getElement('data', $tsAdmin->clientList("-uid -away -voice -times -groups -info -icon -country -ip -badges"));
	$hand = fopen('cache/clientList.json', "w+");
	fwrite($hand, json_encode($cache));
	fclose($hand);
	$cache = $tsAdmin->getElement('data', $tsAdmin->banList());
	$hand = fopen('cache/banList.json', "w+");
	fwrite($hand, json_encode($cache));
	fclose($hand);
	$tsAdmin->logout();
} else {
	if (file_exists('cache/serverInfo.json')) {
		$server = file_get_contents('cache/serverInfo.json');
		$server = json_decode($server, true);
		$server['virtualserver_status'] = 'offline';
		$server['virtualserver_clientsonline'] = 0;
		$server['virtualserver_queryclientsonline'] = 0;
		$hand = fopen('cache/serverInfo.json', "w+");
		fwrite($hand, json_encode($server));
		fclose($hand);
		touch('cache/serverGroupList.json');
		touch('cache/channelGroupList.json');
		touch('cache/channelList.json');
		touch('cache/clientList.json');
		touch('cache/banList.json');
	}
}
$tsAdmin = null;
?>