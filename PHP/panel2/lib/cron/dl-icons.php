<?php
require("../ts3admin/ts3admin.class.php");
require('../../config.php');

$mysqli = new mysqli($db['host'], $db['user'], $db['password'], $db['database']);

if($mysqli->connect_error) {
    die('Error : ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
}
$results = $mysqli->query("SELECT `key`, `value` FROM `avnbot_config` WHERE `key`='ip' OR `key`='query_port' OR `key`='port' OR `key`='login' OR `key`='password' OR `key`='nick'");

while($row = $results->fetch_array()) {
   $config[$row["key"]] = $row["value"];
}

$results->free();
$mysqli->close();

$tsAdmin = new ts3admin($config['ip'], $config['query_port']);
if($tsAdmin->getElement('success', $tsAdmin->connect())) {
	$tsAdmin->login($config['login'], $config['password']);
	$tsAdmin->selectServer($config['port']);
	$tsAdmin->setName($config['nick'].' Icons');
	$icons = $tsAdmin->getElement('data', $tsAdmin->ftGetFileList(0, '','/icons/'));
	foreach($icons as $icon) {
		if(!file_exists('../../img/icons/'.$icon['name'].'.png')) {
			$transfer = $tsAdmin->getElement('data', $tsAdmin->ftInitDownload('/'.$icon['name'], 0, ''));
			$connection_transfer = fsockopen($config['ip'], $transfer['port'], $errnum, $errstr, 10);
			fputs($connection_transfer, $transfer['ftkey']);
			$data = '';
			while(!feof($connection_transfer)) {
				$data .= fgets($connection_transfer, 4096);
			}
			$hand = fopen('../../img/icons/'.$icon['name'].'.png', "w+");
			fwrite($hand, $data);
			fclose($hand);
		}
	}
	$tsAdmin->logout();
}
$tsAdmin = null;
?>