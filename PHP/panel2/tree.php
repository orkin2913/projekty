<?php
ini_set('display_errors',"1");

require('config.php');
require_once('lib/tools/tools.class.php');
require_once('lib/view/view.class.php');
$tools = new tools($db);
$view = new View();

$config = $tools->loadConfig();

//Ładowanie języka
$lang = $tools->loadLang($config['lang']);

//Ładowanie drzewa
$tree = $tools->loadTree();
//$tree = $tools->loadTree('3,5,6');

//Ładowanie cache
$server = $tools->loadCache('serverInfo');


//Status serwera
$server['clients'] = $server['virtualserver_clientsonline'] - $server['virtualserver_queryclientsonline'];
if ($server['virtualserver_status'] == 'online') {
	$server['type'] = 'success';
	$server['virtualserver_uptime'] = $tools->secToTime($server['virtualserver_uptime']); $server['virtualserver_uptime'] = ' '.$lang[105].' <b>'.$server['virtualserver_uptime']['dni'].'<sub>d</sub> '.$server['virtualserver_uptime']['godziny'].'<sub>h</sub> '.$server['virtualserver_uptime']['minuty'].'<sub>m</sub> '.$server['virtualserver_uptime']['sekundy'].'<sub>s</sub></b>';
} else {
	$server['type'] = 'danger';
	$server['virtualserver_uptime'] = '';
}

//Status administracji
$adminStatus = $tools->adminStatus();

$head = '<link rel="stylesheet" type="text/css" href="css/tsstatus.css">
		<script type="text/javascript" src="js/tsstatus.js"></script>';
$bottom = '<script src="js/jquery.animateNumber.min.js"></script>
		<script>
			jQuery(document).ready(function() {
				$(\'.tsstatusItem > a\').each(function(index) {       
					$(this).addClass(\'fadeTree\');
					(function(that, i) { 
						var t = setTimeout(function() { 
							$(that).css(\'visibility\', \'visible\');
							$(that).addClass(\'animated fadeInUp\');
						}, 20 * i);
					})(this, index);
				});
				setTimeout(function(){
					$(\'#slots\').animateNumber({ number: '.$server['clients'].'}, 3000);
					$(\'#channels\').animateNumber({ number: '.$server['virtualserver_channelsonline'].'}, 3000);
				}, 200);
			});
		</script>';
		
$top = $tools->drawMenu();
$view->assign('top', $top);
$view->assign('config', $config);
$view->assign('lang', $lang);
$view->assign('server', $server);
$view->assign('adminStatus', $adminStatus);
$view->assign('title', $lang[200].' | AVNBot Panel');
$view->assign('tree', $tree);
$view->assign('head', $head);
$view->assign('bottom', $bottom);
$view->show('tree.tpl');
?>