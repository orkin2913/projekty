<?php
ini_set('display_errors',"1");

require('config.php');
require_once('lib/tools/tools.class.php');
require_once('lib/view/view.class.php');
$tools = new tools($db);
$view = new View();

$config = $tools->loadConfig();

//Ładowanie języka
$lang = $tools->loadLang($config['lang']);

//Status administracji
$adminStatus = $tools->adminList();

$head = '';
$bottom = '';

$top = $tools->drawMenu();
$view->assign('top', $top);
$view->assign('config', $config);
$view->assign('lang', $lang);
$view->assign('adminStatus', $adminStatus);
$view->assign('title', $lang[300].' | AVNBot Panel');
$view->assign('head', $head);
$view->assign('bottom', $bottom);
$view->show('admins.tpl');
?>