		<div class="container">
			<div class="col-md-9">
				<?php echo $this->tree; ?>
			</div>
			<div class="col-md-3">
				<div class="panel panel-default animated fadeInUp anim-1">
					<!--<div class="panel-heading"><?php echo $this->lang[106]; ?></div>-->
					<div class="panel-body server-status text-center">
						<h4><b><a href="ts3server://<?php echo $this->config['server_public_address']; ?>"><?php echo $this->config['server_public_address']; ?></a></b></h4><br>
						<ul class="list-group">
							<li class="list-group-item"><h5><i class="fa fa-users"></i> <b><big id="slots">0</big><sub>/<?php echo $this->server['virtualserver_maxclients']; ?></sub></b></h5></li>
							<li class="list-group-item"><h5><b id="channels">0</b> <?php echo $this->lang[104]; ?></h5></li>
							<li class="list-group-item"><h5><span class="label label-<?php echo $this->server['type']; ?>"><?php echo $this->server['virtualserver_status']; ?></span><?php echo $this->server['virtualserver_uptime']; ?></h5></li>
						</ul>
					</div>
				</div>
				<div class="panel panel-default animated fadeInUp anim-2">
					<div class="panel-heading"><?php echo $this->lang[107]; ?></div>
					<div class="panel-body">
						<?php echo $this->adminStatus; ?>
					</div>
				</div>
			</div>