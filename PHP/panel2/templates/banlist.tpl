		<div class="container">
			<div class="col-md-12 animated fadeIn anim-2">
			<div class="table-responsive">
				<table id="ranking" class="table table-striped" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>ID</th>
							<th><?php echo $this->lang[501]; ?></th>
							<th>IP</th>
							<th>UID</th>
							<th><?php echo $this->lang[502]; ?></th>
							<th><?php echo $this->lang[503]; ?></th>
							<th><?php echo $this->lang[504]; ?></th>
							<th><?php echo $this->lang[505]; ?></th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<th>ID</th>
							<th><?php echo $this->lang[501]; ?></th>
							<th>IP</th>
							<th>UID</th>
							<th><?php echo $this->lang[502]; ?></th>
							<th><?php echo $this->lang[503]; ?></th>
							<th><?php echo $this->lang[504]; ?></th>
							<th><?php echo $this->lang[505]; ?></th>
						</tr>
					</tfoot>
					<tbody>
						<?php echo $this->tabelka; ?>
					</tbody>
				</table>
			</div>
			</div>