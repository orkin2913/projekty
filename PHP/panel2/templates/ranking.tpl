		<div class="container">
			<div class="col-md-12 animated fadeIn anim-2">
			<div class="table-responsive">
				<table id="ranking" class="table table-striped" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>#</th>
							<th>ID</th>
							<th><?php echo $this->lang[401]; ?></th>
							<th><?php echo $this->lang[402]; ?></th>
							<th><?php echo $this->lang[403]; ?></th>
							<th><?php echo $this->lang[404]; ?></th>
							<th>co</th>
							<th><?php echo $this->lang[405]; ?></th>
							<th>ca</th>
							<th><?php echo $this->lang[406]; ?></th>
							<th>cr</th>
							<th><?php echo $this->lang[407]; ?></th>
							<th>uid</th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<th>#</th>
							<th>ID</th>
							<th><?php echo $this->lang[401]; ?></th>
							<th><?php echo $this->lang[402]; ?></th>
							<th><?php echo $this->lang[403]; ?></th>
							<th><?php echo $this->lang[404]; ?></th>
							<th>co</th>
							<th><?php echo $this->lang[405]; ?></th>
							<th>ca</th>
							<th><?php echo $this->lang[406]; ?></th>
							<th>cr</th>
							<th><?php echo $this->lang[407]; ?></th>
							<th>uid</th>
						</tr>
					</tfoot>
					<tbody>
						<?php echo $this->tabelka; ?>
					</tbody>
				</table>
			</div>
			</div>