<!DOCTYPE html>
<html lang="pl">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<meta name="author" content="AVNTeam">
		<link rel="author" href="http://avnteam.net/">
		<meta name="generator" content="AVNBot">
		<title><?php echo $this->title; ?></title>

		<!-- Bootstrap -->
		<link href="css/<?php echo $this->config['style']; ?>.bootstrap.min.css" rel="stylesheet">
		<link href="css/avnbot-panel.css" rel="stylesheet">
		<link rel="stylesheet" href="css/font-awesome.min.css">
		<link rel="stylesheet" href="css/animate.min.css">
		<?php echo $this->head; ?>

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		<nav class="navbar navbar-default navbar-static-top">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="#"><?php echo $this->config['title']; ?></a>
				</div>
				<div id="navbar" class="navbar-collapse collapse">
					<ul class="nav navbar-nav navbar-left">
						<li><a href="home.php"><?php echo $this->lang['m01']; ?></a></li>
						<li><a href="ranking.php"><?php echo $this->lang['m02']; ?></a></li>
						<li><a href="admins.php"><?php echo $this->lang['m03']; ?></a></li>
						<li><a href="tree.php"><?php echo $this->lang['m04']; ?></a></li>
						<li><a href="banlist.php"><?php echo $this->lang['m05']; ?></a></li>
						<li><a href="#"><?php echo $this->lang['m06']; ?></a></li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<?php echo $this->top; ?>
					</ul>
				</div>
			</div>
		</nav>
