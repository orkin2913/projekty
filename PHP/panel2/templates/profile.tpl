		<div class="container profil">
			<div class="col-md-2 col-xs-12">
				<img src="<?php echo $this->client['avatar']; ?>" alt="avatar" class="img-rounded img-responsive animated fadeIn" id="avatar">
				
				<h4 class="text-center" id="status" style="visibility: hidden;"><span class="label label-<?php echo $this->status['type']; ?>"  data-toggle="tooltip" data-placement="bottom" title="<?php echo $this->status['desc']; ?>"><?php echo $this->status['info']; ?></span></h4>

			</div>
			<div class="col-md-10 col-xs-12">
				<div class="col-md-12 col-xs-12"><h2 class="nick animated fadeIn"><?php echo $this->client['nick']; ?></h2></div>
				<div class="col-md-10 col-xs-10"><p class="profil-span animated fadeIn"><?php echo $this->lang[58].' '.date($this->lang[59], $this->client['created']); ?></p></div>
				<div class="col-md-2 col-xs-2">
					<div class="btn-group pull-right button-akcje animated fadeIn">
						<button type="button" class="btn btn-primary dropdown-toggle btn-xs" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo $this->lang[60]; ?> <span class="caret"></span></button>
						<ul class="dropdown-menu">
							<li><a href="#"><?php echo $this->lang[61]; ?></a></li>
							<li><a href="#"><?php echo $this->lang[62]; ?></a></li>
							<li><a href="#"><?php echo $this->lang[63]; ?></a></li>
							<li><a href="#"><?php echo $this->lang[64]; ?></a></li>
							<li role="separator" class="divider"></li>
							<li><a href="#"><?php echo $this->lang[65]; ?></a></li>
						</ul>
					</div>
				</div>
				<div class="col-md-12 col-xs-12 profil-data-container animated fadeIn" id="tabelka">
					<ul class="list-group profil-data">
						<li class="list-group-item col-md-12 col-xs-12"><div class="col-md-2 col-xs-12"><b><?php echo $this->lang[70]; ?></b></div><div class="col-md-10 col-xs-12"><?php echo $this->groups; ?></div></li>
						<li class="list-group-item col-md-12 col-xs-12"><div class="col-md-2 col-xs-12"><b>UID:</b></div><div class="col-md-10 col-xs-12"><?php echo $this->client['cluid']; ?></div></li>
						<li class="list-group-item col-md-12 col-xs-12"><div class="col-md-2 col-xs-12"><b>DBID:</b></div><div class="col-md-10 col-xs-12"><?php echo $this->client['cldbid']; ?></div></li>
						<li class="list-group-item col-md-12 col-xs-12"><div class="col-md-2 col-xs-12"><b><?php echo $this->lang[71]; ?></b></div><div class="col-md-8 col-xs-10" id="pkt">0</div><div class="col-md-2 col-xs-2 text-right"><span class="label label-<?php echo $this->ranking['pkt_rank']; ?>" data-toggle="tooltip" data-placement="right" title="Ranking">#<span id="pkt-rank">0</span></span></div></li>
						<li class="list-group-item col-md-12 col-xs-12"><div class="col-md-2 col-xs-12"><b><?php echo $this->lang[72]; ?></b></div><div class="col-md-8 col-xs-10" id="totalconnections">0</div><div class="col-md-2 col-xs-2 text-right"><span class="label label-<?php echo $this->ranking['totalconnections_rank']; ?>" data-toggle="tooltip" data-placement="right" title="Ranking">#<span id="totalconnections-rank">0</span></span></div></li>
						<li class="list-group-item col-md-12 col-xs-12"><div class="col-md-2 col-xs-12"><b><?php echo $this->lang[73]; ?></b></div><div class="col-md-8 col-xs-10"><?php echo $this->client['time_online']; ?></div><div class="col-md-2 col-xs-2 text-right"><span class="label label-<?php echo $this->ranking['time_online_rank']; ?>" data-toggle="tooltip" data-placement="right" title="Ranking">#<span id="time_online-rank">0</span></span></div></li>
						<li class="list-group-item col-md-12 col-xs-12"><div class="col-md-2 col-xs-12"><b><?php echo $this->lang[74]; ?></b></div><div class="col-md-8 col-xs-10"><?php echo $this->client['time_afk']; ?> (<?php echo $this->client['time_afk_prc']; ?>%)</div><div class="col-md-2 col-xs-2 text-right"><span class="label label-<?php echo $this->ranking['time_afk_rank']; ?>" data-toggle="tooltip" data-placement="right" title="Ranking">#<span id="time_afk-rank">0</span></span></div></li>
						<li class="list-group-item col-md-12 col-xs-12"><div class="col-md-2 col-xs-12"><b><?php echo $this->lang[75]; ?></b></div><div class="col-md-8 col-xs-10"><?php echo $this->client['record']; ?></div><div class="col-md-2 col-xs-2 text-right"><span class="label label-<?php echo $this->ranking['record_rank']; ?>" data-toggle="tooltip" data-placement="right" title="Ranking">#<span id="record-rank">0</span></span></div></li>
						<li class="list-group-item col-md-12 col-xs-12"><div class="col-md-2 col-xs-12"><b><?php echo $this->lang[76]; ?></b></div><div class="col-md-10 col-xs-12"><?php echo $this->channel['channel_name']; ?></div></li>
						<li class="list-group-item col-md-12 col-xs-12"><div class="col-md-2 col-xs-12"><b>IP:</b></div><div class="col-md-10 col-xs-12"><?php //echo $this->client['ip']; ?> (TYLKO DLA ADMINISTRATORÓW Z UPRAWNIENIAMI DO PODGLĄDANIA IP)</div></li>
					</ul>
				</div>
				<div class="col-md-12 col-xs-12">
					<div class="row">
					<?php echo $this->tree; ?>
					<?php echo $this->cc; ?>
					</div>
				</div>
			</div>