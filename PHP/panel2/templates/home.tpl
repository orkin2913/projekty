		<div class="container">
			<div class="col-md-9">
				<div class="panel panel-default animated fadeInUp">
					<div class="panel-body news-readmore">
						<div class="news">
							<div class="news-header">
								<h3 class="news-title text-center">Nasz najlepszy serwer TeamSpeak 3 wystartował!</h3>
								<div class="news-span text-center"><i class="glyphicon glyphicon-calendar"></i> <?php echo date($this->lang[103]); ?> <i class="glyphicon glyphicon-user"></i> <a href="profile.php?id=2">orkin</a></div>
								<hr>
							</div>
							<div class="news-content">
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec arcu purus, eleifend ut sagittis eget, semper sodales dolor. Donec egestas augue quis nisl egestas aliquam. Morbi eu aliquam eros. Donec consequat dui ac finibus cursus. Nulla quis velit euismod, consequat arcu a, luctus leo. Proin dapibus magna vel odio iaculis luctus. Nam porta tincidunt aliquam. Vestibulum et lorem est. Fusce eu tortor mollis, dictum nunc vitae, eleifend tellus. Cras lacinia, ante a viverra vestibulum, ex quam fermentum mi, non tristique massa orci nec risus. Morbi eget eleifend ex, sit amet pharetra leo. Nullam hendrerit, nisl non dignissim malesuada, mauris magna laoreet ex, vitae cursus tortor dui sed metus. Quisque interdum ex ex, a pretium ante tincidunt et.
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec arcu purus, eleifend ut sagittis eget, semper sodales dolor. Donec egestas augue quis nisl egestas aliquam. Morbi eu aliquam eros. Donec consequat dui ac finibus cursus. Nulla quis velit euismod, consequat arcu a, luctus leo. Proin dapibus magna vel odio iaculis luctus. Nam porta tincidunt aliquam. Vestibulum et lorem est. Fusce eu tortor mollis, dictum nunc vitae, eleifend tellus. Cras lacinia, ante a viverra vestibulum, ex quam fermentum mi, non tristique massa orci nec risus. Morbi eget eleifend ex, sit amet pharetra leo. Nullam hendrerit, nisl non dignissim malesuada, mauris magna laoreet ex, vitae cursus tortor dui sed metus. Quisque interdum ex ex, a pretium ante tincidunt et.
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec arcu purus, eleifend ut sagittis eget, semper sodales dolor. Donec egestas augue quis nisl egestas aliquam. Morbi eu aliquam eros. Donec consequat dui ac finibus cursus. Nulla quis velit euismod, consequat arcu a, luctus leo. Proin dapibus magna vel odio iaculis luctus. Nam porta tincidunt aliquam. Vestibulum et lorem est. Fusce eu tortor mollis, dictum nunc vitae, eleifend tellus. Cras lacinia, ante a viverra vestibulum, ex quam fermentum mi, non tristique massa orci nec risus. Morbi eget eleifend ex, sit amet pharetra leo. Nullam hendrerit, nisl non dignissim malesuada, mauris magna laoreet ex, vitae cursus tortor dui sed metus. Quisque interdum ex ex, a pretium ante tincidunt et.
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec arcu purus, eleifend ut sagittis eget, semper sodales dolor. Donec egestas augue quis nisl egestas aliquam. Morbi eu aliquam eros. Donec consequat dui ac finibus cursus. Nulla quis velit euismod, consequat arcu a, luctus leo. Proin dapibus magna vel odio iaculis luctus. Nam porta tincidunt aliquam. Vestibulum et lorem est. Fusce eu tortor mollis, dictum nunc vitae, eleifend tellus. Cras lacinia, ante a viverra vestibulum, ex quam fermentum mi, non tristique massa orci nec risus. Morbi eget eleifend ex, sit amet pharetra leo. Nullam hendrerit, nisl non dignissim malesuada, mauris magna laoreet ex, vitae cursus tortor dui sed metus. Quisque interdum ex ex, a pretium ante tincidunt et.
							</div>
						</div>
					</div>
				</div>
				<hr class="animated fadeInUp news-hr">
				<div class="panel panel-default animated fadeInUp news-2">
					<div class="panel-body news-readmore">
						<div class="news">
							<div class="news-header">
								<h3 class="news-title text-center">Nasz najlepszy serwer TeamSpeak 3 wystartował!</h3>
								<div class="news-span text-center"><i class="glyphicon glyphicon-calendar"></i> <?php echo date($this->lang[103]); ?> <i class="glyphicon glyphicon-user"></i> <a href="profile.php?id=2">orkin</a></div>
								<hr>
							</div>
							<div class="news-content">
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec arcu purus, eleifend ut sagittis eget, semper sodales dolor. Donec egestas augue quis nisl egestas aliquam. Morbi eu aliquam eros. Donec consequat dui ac finibus cursus. Nulla quis velit euismod, consequat arcu a, luctus leo. Proin dapibus magna vel odio iaculis luctus. Nam porta tincidunt aliquam. Vestibulum et lorem est. Fusce eu tortor mollis, dictum nunc vitae, eleifend tellus. Cras lacinia, ante a viverra vestibulum, ex quam fermentum mi, non tristique massa orci nec risus. Morbi eget eleifend ex, sit amet pharetra leo. Nullam hendrerit, nisl non dignissim malesuada, mauris magna laoreet ex, vitae cursus tortor dui sed metus. Quisque interdum ex ex, a pretium ante tincidunt et.
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec arcu purus, eleifend ut sagittis eget, semper sodales dolor. Donec egestas augue quis nisl egestas aliquam. Morbi eu aliquam eros. Donec consequat dui ac finibus cursus. Nulla quis velit euismod, consequat arcu a, luctus leo. Proin dapibus magna vel odio iaculis luctus. Nam porta tincidunt aliquam. Vestibulum et lorem est. Fusce eu tortor mollis, dictum nunc vitae, eleifend tellus. Cras lacinia, ante a viverra vestibulum, ex quam fermentum mi, non tristique massa orci nec risus. Morbi eget eleifend ex, sit amet pharetra leo. Nullam hendrerit, nisl non dignissim malesuada, mauris magna laoreet ex, vitae cursus tortor dui sed metus. Quisque interdum ex ex, a pretium ante tincidunt et.
							</div>
						</div>
					</div>
				</div>
				<hr class="animated fadeInUp news-hr">
				<div class="panel panel-default animated fadeInUp news-3">
					<div class="panel-body news-readmore">
						<div class="news">
							<div class="news-header">
								<h3 class="news-title text-center">Nasz najlepszy serwer TeamSpeak 3 wystartował!</h3>
								<div class="news-span text-center"><i class="glyphicon glyphicon-calendar"></i> <?php echo date($this->lang[103]); ?> <i class="glyphicon glyphicon-user"></i> <a href="profile.php?id=2">orkin</a></div>
								<hr>
							</div>
							<div class="news-content">
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec arcu purus, eleifend ut sagittis eget, semper sodales dolor. Donec egestas augue quis nisl egestas aliquam. Morbi eu aliquam eros. Donec consequat dui ac finibus cursus. Nulla quis velit euismod, consequat arcu a, luctus leo. Proin dapibus magna vel odio iaculis luctus. Nam porta tincidunt aliquam. Vestibulum et lorem est. Fusce eu tortor mollis, dictum nunc vitae, eleifend tellus. Cras lacinia, ante a viverra vestibulum, ex quam fermentum mi, non tristique massa orci nec risus. Morbi eget eleifend ex, sit amet pharetra leo. Nullam hendrerit, nisl non dignissim malesuada, mauris magna laoreet ex, vitae cursus tortor dui sed metus. Quisque interdum ex ex, a pretium ante tincidunt et.
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="panel panel-default animated fadeInUp anim-1">
					<!--<div class="panel-heading"><?php echo $this->lang[106]; ?></div>-->
					<div class="panel-body server-status text-center">
						<h4><b><a href="ts3server://<?php echo $this->config['server_public_address']; ?>"><?php echo $this->config['server_public_address']; ?></a></b></h4><br>
						<ul class="list-group">
							<li class="list-group-item"><h5><i class="fa fa-users"></i> <b><big id="slots">0</big><sub>/<?php echo $this->server['virtualserver_maxclients']; ?></sub></b></h5></li>
							<li class="list-group-item"><h5><b id="channels">0</b> <?php echo $this->lang[104]; ?></h5></li>
							<li class="list-group-item"><h5><span class="label label-<?php echo $this->server['type']; ?>"><?php echo $this->server['virtualserver_status']; ?></span><?php echo $this->server['virtualserver_uptime']; ?></h5></li>
						</ul>
					</div>
				</div>
				<div class="panel panel-default animated fadeInUp anim-2">
					<div class="panel-heading"><?php echo $this->lang[107]; ?></div>
					<div class="panel-body">
						<?php echo $this->adminStatus; ?>
					</div>
				</div>
			</div>