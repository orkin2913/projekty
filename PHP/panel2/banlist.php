<?php
ini_set('display_errors',"1");
require('config.php');
require_once('lib/tools/tools.class.php');
require_once('lib/view/view.class.php');
$tools = new tools($db);
$view = new View();

$config = $tools->loadConfig();
$tabelka = $tools->loadBans();

//Ładowanie języka
$lang = $tools->loadLang($config['lang']);

$head = '<link rel="stylesheet" type="text/css" href="css/datatables.min.css"/>';
$bottom = '<script type="text/javascript" src="js/datatables.min.js"></script>
<script>
	$(document).ready(function() {
		$(\'#ranking\').DataTable( {
			"order": [[ 0, "desc" ]],
			"language": {
				"lengthMenu": "'.$lang[450].'",
				"zeroRecords": "'.$lang[451].'",
				"info": "'.$lang[452].'",
				"infoEmpty": "'.$lang[453].'",
				"infoFiltered": "'.$lang[454].'",
				"emptyTable": "'.$lang[453].'",
				"search": "'.$lang[455].'",
				"paginate": {
					"first": "'.$lang[456].'",
					"last": "'.$lang[457].'",
					"next": "'.$lang[458].'",
					"previous": "'.$lang[459].'"
				},
			},
			"columnDefs": [
				{ "orderable": false, "targets": 1 },
				{ "orderable": false, "targets": 2 },
				{ "orderable": false, "targets": 3 },
				{ "orderable": false, "targets": 4 },
				{ "orderable": false, "targets": 5 },
				{ "orderable": false, "targets": 6 },
				{ "orderable": false, "targets": 7 }
			]
		} );
	});
</script>';

$top = $tools->drawMenu();
$view->assign('top', $top);
$view->assign('config', $config);
$view->assign('lang', $lang);
$view->assign('tabelka', $tabelka);
$view->assign('title', $lang[500].' | AVNBot Panel');
$view->assign('head', $head);
$view->assign('bottom', $bottom);
$view->show('banlist.tpl');
?>