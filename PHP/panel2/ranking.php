<?php
ini_set('display_errors',"1");
require('config.php');
require_once('lib/tools/tools.class.php');
require_once('lib/view/view.class.php');
$tools = new tools($db);
$view = new View();

$config = $tools->loadConfig();
$tabelka = $tools->loadAllClients();

//Ładowanie języka
$lang = $tools->loadLang($config['lang']);

$head = '<link rel="stylesheet" type="text/css" href="css/datatables.min.css"/>';
$bottom = '<script type="text/javascript" src="js/datatables.min.js"></script>
<script>
	$(document).ready(function() {
		var t = $(\'#ranking\').DataTable( {
			"order": [[ 5, "desc" ]],
			"language": {
				"lengthMenu": "'.$lang[450].'",
				"zeroRecords": "'.$lang[451].'",
				"info": "'.$lang[452].'",
				"infoEmpty": "'.$lang[453].'",
				"infoFiltered": "'.$lang[454].'",
				"emptyTable": "'.$lang[453].'",
				"search": "'.$lang[455].'",
				"paginate": {
					"first": "'.$lang[456].'",
					"last": "'.$lang[457].'",
					"next": "'.$lang[458].'",
					"previous": "'.$lang[459].'"
				},
			},
			"columnDefs": [
				{ "orderData":[ 6 ],   "targets": [ 7 ] },
				{ "orderData":[ 8 ],   "targets": [ 9 ] },
				{ "orderData":[ 10 ],   "targets": [ 11 ] },
				{
					"targets": [ 0 ],
					"orderable": false,
					"searchable": false
				},
				{
					"targets": [ 3 ],
					"searchable": false
				},
				{
					"targets": [ 4 ],
					"searchable": false
				},
				{
					"targets": [ 5 ],
					"searchable": false
				},
				{
					"targets": [ 6 ],
					"visible": false,
					"searchable": false
				},
				{
					"targets": [ 7 ],
					"searchable": false
				},
				{
					"targets": [ 8 ],
					"visible": false,
					"searchable": false
				},
				{
					"targets": [ 9 ],
					"searchable": false
				},
				{
					"targets": [ 10 ],
					"visible": false,
					"searchable": false
				},
				{
					"targets": [ 11 ],
					"searchable": false
				},
				{
					"targets": [ 12 ],
					"visible": false,
					"searchable": true
				}
			]
		} );
		t.on( \'order.dt search.dt\', function () {
			t.column(0, {search:\'applied\', order:\'applied\'}).nodes().each( function (cell, i) {
				cell.innerHTML = \'<b>\'+(i+1)+\'</b>\';
			} );
		} ).draw();
	});
</script>';

$top = $tools->drawMenu();
$view->assign('top', $top);
$view->assign('config', $config);
$view->assign('lang', $lang);
$view->assign('tabelka', $tabelka);
$view->assign('title', $lang[400].' | AVNBot Panel');
$view->assign('head', $head);
$view->assign('bottom', $bottom);
$view->show('ranking.tpl');
?>