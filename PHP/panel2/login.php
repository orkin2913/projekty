<?php
ini_set('display_errors',"1");

if (isset($_POST['code'])) $postCode =  $_POST['code'];
if (isset($_POST['cldbid'])) $postCldbid = $_POST['cldbid'];
if (isset($_POST['clid'])) $postClid = $_POST['clid'];

require('config.php');
require_once('lib/tools/tools.class.php');
require_once('lib/view/view.class.php');
$tools = new tools($db);
$view = new View();

	if (!isset($_SESSION['cldbid'])) {
			
		$config = $tools->loadConfig();

		//Ładowanie języka
		$lang = $tools->loadLang($config['lang']);

		$bott = ''; $loginform = '';
		//Czy wpisano kod logowania?
		if ((!isset($postCode)) && (!isset($postCldbid))) {

			//Ładowanie cache
			$clients = $tools->loadCache('clientList');

			$c = array();
			foreach ($clients as $client) {
				if ($client['connection_client_ip'] == $_SERVER['REMOTE_ADDR']) {
					array_push($c, $client);
				}
			}
			
			//Jeżeli nie połączony z serwerem lub brak w cache
			if (count($c) == 0) $bott = '<script src="js/circle-progress.js"></script><script>$(".login").css("display","inline");$(".circle").css("display","inline-block");$(".login-cover").css("display","inline");var interval='.$config['update_cache'].';var i=interval;$(\'.circle\').circleProgress({value:1,size:150,startAngle:-Math.PI/2,reverse:!0,lineCap:"round",fill:{gradient:["#FF8000","#FFB300","#FFCD00"]},emptyFill:"rgba(0, 0, 0, .0)",animation:{duration:60000},animationStartValue:1}).on(\'circle-animation-progress\',function(event,progress,stepValue){$(this).find(\'strong\').text(i)});setInterval(function(){i=i-1;if(i<=0)location.reload();if(i>=0)$(\'#seconds-left\').text(i)},1000);$(\'.circle\').circleProgress(\'value\',0);</script>';
			//Jeżeli więcej niż jeden client o tym samym IP
			elseif (count($c) > 1) {
				//Jeżeli wybrano właściwego clienta
				if (isset($postClid)) {
					$c = $c[$postClid];
					$genC = $tools->genCode($c['client_database_id']);
					$loginform = '<h3><span class="animated fadeIn hi">'.$lang[703].'</span> <span class="animated fadeIn hi-nick">'.$c['client_nickname'].'!</span></h3><br><h5><span class="animated fadeIn hi-first">'.$lang[704].'</span><br><span class="animated fadeIn hi-second">'.$lang[705].'</span></h5><br>
				<div class="col-md-4 col-md-offset-4 animated fadeIn hi-form"><form method="post" action=""><div class="col-md-8"><input type="text" id="cldbid" name="cldbid" style="display:none;" value="'.$c['client_database_id'].'"><input type="password" class="form-control" maxlength=6 id="code" name="code"></div><div class="col-md-4"><input class="btn btn-default" href="#" type="submit" value="'.$lang[707].'"></div></form><br><br><h5><small>'.$lang[706].'</small></h5></div>';
					if ($genC == true)
						$bott = '<script>setTimeout(function() {$.ajax({
						url: \'scripts/send-code.php\',
						type: \'post\',
						data: { "cldbid": "'.$c['client_database_id'].'"},
						success: function(response) {  }
						});
						}, 4500);</script>';
					
				//Jeżeli jeszcze nie wybrano właściwego clienta
				} else {
					$loginform = '<h3><span class="animated fadeIn hi">'.$lang[703].'!</span></h3><br><h5><span class="animated fadeIn hi-first">'.$lang[709].'</span><br><span class="animated fadeIn hi-second">'.$lang[710].'</span></h5><br>';
					$loginform .= '<div class="col-md-4 col-md-offset-4 animated fadeIn hi-form"><form method="post" action="">';
					
					$length = count($c);
					for ($i = 0; $i < $length; $i++) {
						$loginform .= '<div class="col-md-6"><div class="radio"><label><input type="radio" name="clid" id="clid1" value="'.$i.'">'.$c[$i]['client_nickname'].' (cldbid: '.$c[$i]['client_database_id'].')</label></div></div>';
					}
					$loginform .= '<input class="btn btn-default" href="#" type="submit" value="'.$lang[707].'"></form></div>';
					
				}
					
			//Jeżeli jeden client
			} else {
				$c = $c[0];
				$genC = $tools->genCode($c['client_database_id']);
				$loginform = '<h3><span class="animated fadeIn hi">'.$lang[703].'</span> <span class="animated fadeIn hi-nick">'.$c['client_nickname'].'!</span></h3><br><h5><span class="animated fadeIn hi-first">'.$lang[704].'</span><br><span class="animated fadeIn hi-second">'.$lang[705].'</span></h5><br>
			<div class="col-md-4 col-md-offset-4 animated fadeIn hi-form"><form method="post" action=""><div class="col-md-8"><input type="text" id="cldbid" name="cldbid" style="display:none;" value="'.$c['client_database_id'].'"><input type="password" class="form-control" maxlength=6 id="code" name="code"></div><div class="col-md-4"><input class="btn btn-default" href="#" type="submit" value="'.$lang[707].'"></div></form><br><br><h5><small>'.$lang[706].'</small></h5></div>';
				if ($genC == true)
					$bott = '<script>setTimeout(function() {$.ajax({
					url: \'scripts/send-code.php\',
					type: \'post\',
					data: { "cldbid": "'.$c['client_database_id'].'"},
					success: function(response) {  }
					});
					}, 4500);</script>';
			}
			
		//Jeżeli wpisano kod logowania
		} else {
			if ($tools->checkCode($postCode, $postCldbid)) {
				
				$client = $tools->loadClient($postCldbid);
				
				$_SESSION['cldbid'] = $postCldbid;
				$_SESSION['nick'] = $client['nick'];
				if ($client['avatar'] == false)
					$_SESSION['avatar'] = 'img/avatar.gif';
				else $_SESSION['avatar'] = 'img/avatars/avatar_'.$client['avatar'];
				
				//Tutaj nawpisywać różnych rzeczy. Np. czy jest adminem i czy ma uprawnienia do kicków, wyświetlania IP itp :D
				
				header("Location: index.php");
				
			//Błędny kod
			} else  {
				$loginform = '<h3><span class="animated fadeIn hi">'.$lang[708].'</span> </h3>';
			}
		}

		$head = '';
		$bottom = $bott;
				
		$view->assign('config', $config);
		$view->assign('lang', $lang);
		$view->assign('loginform', $loginform);
		$view->assign('title', $lang[700].' | AVNBot Panel');
		$view->assign('head', $head);
		$view->assign('bottom', $bottom);
		$view->assign('top', '');
		$view->show('login.tpl');
	} else {
		header("Location: index.php");
	}
?>