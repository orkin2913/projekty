<?php
ini_set('display_errors',"1");
require('config.php');
require_once('lib/tools/tools.class.php');
require_once('lib/view/view.class.php');
$tools = new tools($db);
$view = new View();

$config = $tools->loadConfig();

//Ładowanie języka
$lang = $tools->loadLang($config['lang']);

//Ładowanie cache
$server = $tools->loadCache('serverInfo');


//Status serwera
$server['clients'] = $server['virtualserver_clientsonline'] - $server['virtualserver_queryclientsonline'];
if ($server['virtualserver_status'] == 'online') {
	$server['type'] = 'success';
	$server['virtualserver_uptime'] = $tools->secToTime($server['virtualserver_uptime']); $server['virtualserver_uptime'] = ' '.$lang[105].' <b>'.$server['virtualserver_uptime']['dni'].'<sub>d</sub> '.$server['virtualserver_uptime']['godziny'].'<sub>h</sub> '.$server['virtualserver_uptime']['minuty'].'<sub>m</sub> '.$server['virtualserver_uptime']['sekundy'].'<sub>s</sub></b>';
} else {
	$server['type'] = 'danger';
	$server['virtualserver_uptime'] = '';
}

//Status administracji
$adminStatus = $tools->adminStatus();


$head = '';
$bottom = '<script src="js/readmore.min.js"></script>
		<script src="js/jquery.animateNumber.min.js"></script>
		<script>
			jQuery(document).ready(function() {
				$(\'.news-readmore\').readmore({
					speed: 900,
					collapsedHeight: 460,
					moreLink: \'<div class="panel-footer news-footer text-center"><button type="button" class="btn btn-link btn-xs"><i class="glyphicon glyphicon-chevron-down"></i> Pokaż wiecej</button></div>\',
					lessLink: \'<div class="panel-footer news-footer text-center"><button type="button" class="btn btn-link btn-xs"><i class="glyphicon glyphicon-chevron-up"></i> Pokaż wiecej</button></div>\'
				});
				setTimeout(function(){
					$(\'#slots\').animateNumber({ number: '.$server['clients'].'}, 3000);
					$(\'#channels\').animateNumber({ number: '.$server['virtualserver_channelsonline'].'}, 3000);
				}, 200);
			});
		</script>
';


$top = $tools->drawMenu();
$view->assign('top', $top);
$view->assign('lang', $lang);
$view->assign('server', $server);
$view->assign('adminStatus', $adminStatus);
$view->assign('title', $lang[100].' | AVNBot Panel');
$view->assign('config', $config);
$view->assign('head', $head);
$view->assign('bottom', $bottom);
$view->show('home.tpl');
?>