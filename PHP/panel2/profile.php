<?php
ini_set('display_errors',"1");

//Sprawdzanie czy podano ID w adresie
if (!empty($_GET['id'])) {
	$cldbid = $_GET['id'];
} else {
	//Jak nie podano, to przekieruj na główną
	header("Location: index.php");
	die();
}
require('config.php');
require_once('lib/tools/tools.class.php');
require_once('lib/view/view.class.php');
$tools = new tools($db);
$view = new View();

$config = $tools->loadConfig();
//Ładowanie danych o cliencie
$client = $tools->loadClient($cldbid);
if (empty($client)) {
	//Jak nie ma usera w bazie, to przekieruj na główną
	header("Location: index.php");
	die();
}

//Ładowanie języka
$lang = $tools->loadLang($config['lang']);

//Ładowanie cache
$serverInfo = $tools->loadCache('serverInfo');
$serverGroupList = $tools->loadCache('serverGroupList');
$clientList = $tools->loadCache('clientList');

//Jeżeli serwer offline
if (($serverInfo['virtualserver_status'] == 'offline') && ($client['status'] != 3))
	$client['status'] = 0;

//Avatar
if ($client['avatar'] == false)
	$client['avatar'] = 'img/avatar.gif';
else $client['avatar'] = 'img/avatars/avatar_'.$client['avatar'];

//Status pod avatarem
//Online
if ($client['status'] == 1) {
	$status['desc'] = $tools->secToTime($client['status_desc']); $status['desc'] = $lang[54].' '.$status['desc']['dni'].'d '.$status['desc']['godziny'].'h '.$status['desc']['minuty'].'m '.$status['desc']['sekundy'].'s';
	$status['type'] = "success";
	$status['info'] = $lang[50];
	
//Away
} elseif ($client['status'] == 2) {
	$status['desc'] = $tools->secToTime($client['status_desc']); $status['desc'] = $lang[54].' '.$status['desc']['dni'].'d '.$status['desc']['godziny'].'h '.$status['desc']['minuty'].'m '.$status['desc']['sekundy'].'s';
	$status['type'] = "warning";
	$status['info'] = $lang[51];
	
//Banned
} elseif ($client['status'] == 3) {
	$status['desc'] = $client['status_desc']; $status['desc'] = $lang[55].' '.$status['desc'];
	$status['type'] = "danger";
	$status['info'] = $lang[53];
	
//Offline
} elseif ($client['status'] == 0) {
	$status['desc'] = $lang[56].' '.date($lang[57], $client['last_seen']);
	$status['type'] = "danger";
	$status['info'] = $lang[52];
	
	$bans = $tools->loadCache("banList");
	if (!empty($bans)) {
		foreach ($bans as $ban) {
			if ($ban['uid'] == $client['cluid']) {
				$tools->fixBan($client['cluid'], $ban['reason']);
				$status['desc'] = $client['status_desc']; $status['desc'] = $lang[55].' '.$ban['reason'];
				$status['type'] = "danger";
				$status['info'] = $lang[53];
				break;
			}
		}
	}
}

//Statystyki
//Grupy
$client['groups'] = explode(',', $client['groups']);
$groups = '';
foreach ($serverGroupList as $group) {
	if (in_array($group['sgid'], $client['groups'])) {
		if ($group['iconid'] < 0) $group['iconid'] = 4294967296 + $group['iconid'];
		$groups .= '<img src="img/icons/icon_'.$group['iconid'].'.png" data-toggle="tooltip" data-placement="top" title="'.$group['name'].'" height="16px" wigth="16px" class="fadeGroups" style="visibility: hidden;">&nbsp;&nbsp;';
	}
}
//Ranking
$tmp = array('pkt_rank', 'totalconnections_rank', 'time_online_rank', 'time_afk_rank', 'record_rank');
foreach ($tmp as $rank) {
	if ($client[$rank] == 1) $ranking[$rank] = 'success';
	elseif ($client[$rank] == 2) $ranking[$rank] = 'info';
	elseif ($client[$rank] == 3) $ranking[$rank] = 'primary';
	else $ranking[$rank] = 'default';
}
//Czas online, czas AFK, rekord
$client['time_afk_prc'] = round($client['time_afk']/$client['time_online'], 2)*100;
$client['time_online'] = $tools->secToTime($client['time_online']); $client['time_online'] = $client['time_online']['dni'].'d '.$client['time_online']['godziny'].'h '.$client['time_online']['minuty'].'m '.$client['time_online']['sekundy'].'s';
$client['time_afk'] = $tools->secToTime($client['time_afk']); $client['time_afk'] = $client['time_afk']['dni'].'d '.$client['time_afk']['godziny'].'h '.$client['time_afk']['minuty'].'m '.$client['time_afk']['sekundy'].'s';
$client['record'] = $tools->secToTime($client['record']); $client['record'] = $client['record']['dni'].'d '.$client['record']['godziny'].'h '.$client['record']['minuty'].'m '.$client['record']['sekundy'].'s';

//Kanał usera
$channel = $tools->clientChannel($cldbid);
$tree = '';
if ($channel['channel_name'] == false) {
	$channel['channel_name'] = $lang[77];
} else {
	if ($channel['sub']) {
		$channel['cid'] .= ','.$channel['sub'];
	}
	$width = 6;
	if (($client['status'] == 0) || ($client['status'] == 3)) $width = 12;
	$tree = '<div class="col-md-'.$width.' col-xs-12" style="visibility: hidden" id="leftChannel">
						<div class="panel panel-default">
							<div class="panel-heading">'.$lang[90].'</div>
							<div class="panel-body">
								'.$tools->loadTree($channel['cid'], 'client_'.$cldbid).'
							</div>
						</div>
					</div>';
}

//Kanał, na którym aktualnie siedzi
$cc = '';
if (($client['status'] == 1) || ($client['status'] == 2)) {
	foreach ($clientList as $c) {
		if ($c['client_database_id'] == $cldbid) {
			$ccid = $c['cid'];
			$width = 6;
			if ($channel['channel_name'] == $lang[77]) $width = 12;
			$cc = '<div class="col-md-'.$width.' col-xs-12" style="visibility: hidden" id="rightChannel">
						<div class="panel panel-default">
							<div class="panel-heading">'.$lang[91].'</div>
							<div class="panel-body">
								'.$tools->loadTree($ccid, 'channel_'.$ccid, true).'
							</div>
						</div>
					</div>';
			break;
		}
	}
}

//Ilość clienktów w bazie
$clientsCount = $tools->clientsCount();

$head = '<link rel="stylesheet" type="text/css" href="css/tsstatus.css">
		<script type="text/javascript" src="js/tsstatus.js"></script>';
$bottom = '<script src="js/jquery.animateNumber.min.js"></script>
		<script>
			$(function () {
				$(\'[data-toggle="tooltip"]\').tooltip()
			});
			$(\'#avatar\').one(\'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend\', function() {
				$(\'#status\').css(\'visibility\', \'visible\');
				$(\'#status\').addClass(\'animated fadeIn\');
			});
			$(\'#tabelka\').one(\'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend\', function() {
				$(\'.fadeGroups\').each(function(index) {        
					(function(that, i) { 
						var t = setTimeout(function() { 
							$(that).css(\'visibility\', \'visible\');
							$(that).addClass(\'animated bounceIn\');
						}, 200 * i);
					})(this, index);
				});
				
				$(\'#leftChannel\').css(\'visibility\', \'visible\');
				$(\'#leftChannel\').addClass(\'animated bounceInLeft\');
				$(\'#rightChannel\').css(\'visibility\', \'visible\');
				$(\'#rightChannel\').addClass(\'animated bounceInRight\');
				
			});
			$(\'#pkt\').animateNumber({ number: '.$client['pkt'].'}, 3000);
			$(\'#pkt-rank\').prop(\'number\', '.($clientsCount + 100).').animateNumber({ number: '.$client['pkt_rank'].'}, 3000);
			$(\'#totalconnections\').animateNumber({ number: '.$client['totalconnections'].'}, 3000);
			$(\'#totalconnections-rank\').prop(\'number\', '.($clientsCount + 100).').animateNumber({ number: '.$client['totalconnections_rank'].'}, 3000);
			$(\'#time_online-rank\').prop(\'number\', '.($clientsCount + 100).').animateNumber({ number: '.$client['time_online_rank'].'}, 3000);
			$(\'#time_afk-rank\').prop(\'number\', '.($clientsCount + 100).').animateNumber({ number: '.$client['time_afk_rank'].'}, 3000);
			$(\'#record-rank\').prop(\'number\', '.($clientsCount + 100).').animateNumber({ number: '.$client['record_rank'].'}, 3000);
		</script>
';

$top = $tools->drawMenu();
$view->assign('top', $top);
$view->assign('groups', $groups);
$view->assign('ranking', $ranking);
$view->assign('status', $status);
$view->assign('lang', $lang);
$view->assign('serverInfo', $serverInfo);
$view->assign('title', $client['nick'].' - '.$lang[0].' | AVNBot Panel');
$view->assign('config', $config);
$view->assign('client', $client);
$view->assign('channel', $channel);
$view->assign('tree', $tree);
$view->assign('cc', $cc);
$view->assign('head', $head);
$view->assign('bottom', $bottom);
$view->show('profile.tpl');
?>