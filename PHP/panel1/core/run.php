<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
$ip = $_SERVER['REMOTE_ADDR'];
if (isset($_SERVER["HTTP_CF_CONNECTING_IP"]) && !empty($_SERVER["HTTP_CF_CONNECTING_IP"])) $ip = $_SERVER["HTTP_CF_CONNECTING_IP"];

//Sprawdzanie czy przesłano dane do skryptu
if ( ( isset($_POST['command']) && !empty($_POST['command']) && isset($_POST['data']) ) || ( isset($_GET['command']) && !empty($_GET['command']) && isset($_GET['data']) ) ) {
	//Przypisywanie danych do zmiennych
	if (isset($_POST['command'])) {
		$command = $_POST['command'];
		$data = $_POST['data'];
	} else {
		$command = $_GET['command'];
		$data = $_GET['data'];
	}
	
	//Podłączanie konfiguracji i klasy
	require("config.php");
	require("lib/ts3admin.class.php");
	$tsAdmin = new ts3admin($config['ip'], $config['queryport']);
	
	//Łączenie z serwerem
	if($tsAdmin->getElement("success", $tsAdmin->connect())) {
		$tsAdmin->login($config['user'], $config['pass']);
		$tsAdmin->selectServer($config['port']);
		$isAdmin = false; $invokerAdminData  = "";
		$clients = $tsAdmin->getElement("data", $tsAdmin->clientList("-uid -away -voice -times -groups -info -country -icon -ip -badges"));
		foreach ($clients as $client) {
			$client['client_servergroups'] = explode(',', $client['client_servergroups']);
			foreach ($client['client_servergroups'] as $group) {
				if (in_array($group, $config['admins']) && $ip == $client['connection_client_ip']) {
					$invokerAdminData = $client;
					$isAdmin = true;
					break 2;
				}
			}
		}
		if (!$isAdmin) {
			header("Location: http://www.mpcforum.pl/");
			die();
		} else {
			//Sprawdzanie co jest do zrobienia
			//Tworzenie kanałów prywatnych
			if ($command == "channelPrivCreate") {
				createChannel($tsAdmin, $config['privParent'], $data);
				$logTXT = date("\[Y-m-d H:i:s\]")." ".$invokerAdminData['client_nickname']." (".$invokerAdminData['client_unique_identifier'].") utworzył ".$data." kanałów prywatnych";
			
			//Tworzenie kanałów gildijnych
			} elseif ($command == "channelClanCreate") {
				createChannel($tsAdmin, $config['clanParent'], $data);
				$logTXT = date("\[Y-m-d H:i:s\]")." ".$invokerAdminData['client_nickname']." (".$invokerAdminData['client_unique_identifier'].") utworzył ".$data." kanałów gildijnych";
			
			//Wyświetlanie pustych kanałów
			} elseif ($command == "showEmpty") {
				//Pobieranie listy
				$data = $tsAdmin->getElement("data", $tsAdmin->channelList("-topic -flags -voice -limits -icon -secondsempty"));
				$out = array();
				//Wyszukiwanie kanałów w strefie
				foreach ($data as $key => $value) {
					if ($data[$key]['pid'] != $config['privParent'] && $data[$key]['pid'] != $config['clanParent']) {
						unset($data[$key]);
						
					} else {
						//Szukanie właścicieli kanałów
						$owner = $tsAdmin->channelGroupClientList($data[$key]['cid'], NULL, $config['channelAdmin']);
						if (!$owner['success']) {
							$owner['data'][0]['cldbid'] = "";
						} else {
							$owner = $tsAdmin->clientDbInfo($owner['data'][0]['cldbid']);
							$owner['data'][0]['cldbid'] = $owner['data']['client_nickname'].' ('.$owner['data']['client_unique_identifier'].')';
						}
						
						//Dodawanie danych o włascicielu
						$data[$key]['owner'] = $owner['data'][0]['cldbid'];
						array_push($out, $data[$key]);
					}
				}
				$ret = json_encode($out);
				
			//Zwalnianie kanału
			} elseif ($command == "freeChannel") {
				freeChannel($tsAdmin, $data, $config);
				$logTXT = date("\[Y-m-d H:i:s\]")." ".$invokerAdminData['client_nickname']." (".$invokerAdminData['client_unique_identifier'].") zwolnił kanał o id: ".$data;
				
			//Wyświetlanie wszystkich online, którzy nie mają nigdzie admina
			} elseif ($command == "showUsersNoChannel") {
				$ret = array();
				$clients = $tsAdmin->getElement("data", $tsAdmin->clientList("-uid -away -voice -times -groups -info -country -icon -ip -badges"));
				foreach ($clients as $client => $data) {
					if ($tsAdmin->getElement("data", $tsAdmin->channelGroupClientList(NULL, $clients[$client]['client_database_id'], $config['channelAdmin'])) || $clients[$client]['client_type'] == 1)
						unset($clients[$client]);
				}
				$ret = json_encode($clients);
			
			//Nadawanie kanału
			} elseif ($command == "giveChannel") {		
				giveChannel($tsAdmin, $data, $config);
				$logTXT = date("\[Y-m-d H:i:s\]")." ".$invokerAdminData['client_nickname']." (".$invokerAdminData['client_unique_identifier'].") nadał kanał o id: ".$data['cid']." użytkownikowi: ".$data['nick']." (cldbid: ".$data['cldbid'].")";
				
			} else {
				$ret = "Niepoprawne polecenie";
			}
			
			
			
			
			//Zwracanie informacji o poprawnym wykonaniu skryptu
			if (!isset($ret)) {
				$ret = "1";
			}
			if (isset($logTXT) && !empty($logTXT)) {
				file_put_contents('logs.txt', $logTXT.PHP_EOL , FILE_APPEND | LOCK_EX);
			}
		}
	}
	
	
	//Wyświetlanie błędów jeżeli wystąpiły
	if(count($tsAdmin->getDebugLog()) > 0) {
		$ignore = array(
			"Error in channelGroupClientList() on line 811: ErrorID: 1281 | Message: database empty result set",
			"Error in channelEdit() on line 619: ErrorID: 771 | Message: channel name is already in use",
			"Error in clientMove() on line 1880: ErrorID: 770 | Message: already member of channel"
		);
		foreach($tsAdmin->getDebugLog() as $logEntry) {
			if (!in_array($logEntry, $ignore))
				$ret = $logEntry;
		}
	}

	print_r( $ret );	
	
	
} else {
	header("Location: http://www.mpcforum.pl/");
	die();
}

function createChannel($tsAdmin, $parent, $data) {
	//Pobieranie listy kanałów
	$channels = $tsAdmin->getElement("data", $tsAdmin->channelList("-topic -flags -voice -limits -icon -secondsempty"));
	$i = 0; $count = count($channels);
	
	//Liczenie kanałów w strefie
	foreach ($channels as $channel) {
		if ($channel['pid'] == $parent) {
			$i++;
		}
	}
	
	//Tworzenie kanałów
	for ($j = 0; $j < $data; $j++) {
		$i++;
		
		//Ustawienia domyślne tworzonego kanału
		$channelSettings = array(
			"CHANNEL_NAME" => $i.". WOLNY",
			"CHANNEL_FLAG_PERMANENT" => 1,
			"CPID" => $parent,
			"CHANNEL_CODEC" => 4,
			"CHANNEL_CODEC_QUALITY" => 10,
			"CHANNEL_FLAG_MAXCLIENTS_UNLIMITED" => 0,
			"CHANNEL_MAXCLIENTS" => 0
		);
		
		$channelPerms = array(
			"i_channel_needed_delete_power" => 99,
			"i_channel_needed_modify_power" => 70,
			"i_ft_needed_file_upload_power" => 99,
			"i_ft_needed_file_download_power" => 99,
			"i_ft_needed_file_delete_power" => 99,
			"i_ft_needed_file_rename_power" => 99,
			"i_ft_needed_file_browse_power" => 99,
			"i_ft_needed_directory_create_power" => 99
		);
		
		
		//Tworzenie kanału
		$cid = $tsAdmin->getElement("data", $tsAdmin->channelCreate($channelSettings));
		$cid = $cid['cid'];
		
		//Ustawianie uprawnień
		$tsAdmin->channelAddPerm($cid, $channelPerms);
		$cpid = $cid;
		
		//Tworzenie podkanałów
		for ($z = 1; $z <= 2; $z++) {
			$channelSettings['CHANNEL_NAME'] = "#".$z;
			$channelSettings['CPID'] = $cpid;
			
			$cid = $tsAdmin->getElement("data", $tsAdmin->channelCreate($channelSettings));
			$cid = $cid['cid'];
			
			$tsAdmin->channelAddPerm($cid, $channelPerms);
		}
		
	}
}

function freeChannel($tsAdmin, $data, $config) {
	//Sprawdzanie właściciela kanału
	$owner = $tsAdmin->channelGroupClientList($data, NULL, $config['channelAdmin']);
	$parent = $tsAdmin->channelInfo($data); $parent = $parent['data']['pid'];
	//Zdejmowanie rangi
	$test = $tsAdmin->channelGroupAddClient($config['channelGuest'], $data, $owner['data'][0]['cldbid']);
	
	//Pobieranie listy kanałów
	$channels = $tsAdmin->getElement("data", $tsAdmin->channelList("-topic -flags -voice -limits -icon -secondsempty"));
	$i = 0; $z = 0;
	//Liczenie kanałów w strefie
	foreach ($channels as $channel) {
		if ($channel['pid'] == $parent) {
			$i++;
		}
		if ($channel['cid'] == $data)
			break;
	}
	
	//Ustawienia domyślne kanału
	$channelSettings = array(
		"CHANNEL_FLAG_PERMANENT" => 1,
		"CPID" => $parent,
		"CHANNEL_PASSWORD" => "",
		"CHANNEL_CODEC" => 4,
		"CHANNEL_CODEC_QUALITY" => 10,
		"CHANNEL_FLAG_MAXCLIENTS_UNLIMITED" => 0,
		"CHANNEL_MAXCLIENTS" => 0
	);
	
	$channelPerms = array(
		"i_channel_needed_delete_power" => 99,
		"i_channel_needed_modify_power" => 70,
		"i_ft_needed_file_upload_power" => 99,
		"i_ft_needed_file_download_power" => 99,
		"i_ft_needed_file_delete_power" => 99,
		"i_ft_needed_file_rename_power" => 99,
		"i_ft_needed_file_browse_power" => 99,
		"i_ft_needed_directory_create_power" => 99
	);
	
	//Ustawianie domyslnych danych na kanał
	$tsAdmin->channelEdit($data, array("CHANNEL_NAME" => $i.". WOLNY"));
	$tsAdmin->channelEdit($data, $channelSettings);
	$tsAdmin->channelAddPerm($data, $channelPerms);
	
	//Ustawianie podkanałów
	foreach ($channels as $channel) {
		if ($channel['pid'] == $data) {
			$z++;
			$channelSettings['CPID'] = $data;
			$tsAdmin->channelEdit($channel['cid'], array("CHANNEL_NAME" => "#".$z));
			$tsAdmin->channelEdit($channel['cid'], $channelSettings);
			$tsAdmin->channelAddPerm($channel['cid'], $channelPerms);
		}
	}
	
}

function giveChannel($tsAdmin, $data, $config) {
	$i = 0;
	//Pobieranie listy kanałów
	$channels = $tsAdmin->getElement("data", $tsAdmin->channelList("-topic -flags -voice -limits -icon -secondsempty"));
	$parent = $tsAdmin->channelInfo($data['cid']); $parent = $parent['data']['pid'];
	//Liczenie kanałów w strefie
	foreach ($channels as $channel) {
		if ($channel['pid'] == $parent) {
			$i++;
		}
		if ($channel['cid'] == $data['cid'])
			break;
	}
	$tsAdmin->clientMove($data['clid'], $data['cid']);
	$channelSettings = array(
		"CHANNEL_NAME" => $i.". Kanał użytkownika ".$data['nick'],
		"CHANNEL_FLAG_PERMANENT" => 1,
		"CHANNEL_CODEC" => 4,
		"CHANNEL_CODEC_QUALITY" => 10,
		"CHANNEL_FLAG_MAXCLIENTS_UNLIMITED" => 1,
		"CHANNEL_MAXCLIENTS" => -1
	);
	$tsAdmin->channelEdit($data['cid'], $channelSettings);
	$test = $tsAdmin->channelGroupAddClient($config['channelAdmin'], $data['cid'], $data['cldbid']);
}


?>