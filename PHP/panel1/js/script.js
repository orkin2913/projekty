
/**
  * Obiekt zawierający metody obsługujące formularze
  *
  * @author     Orkin (AVNTeam.net)
  */
	function Form() {
		var self = this;
		var giveThisChannel = {
			id: 0,
			cldbid: 0,
			clid: 0,
			nick: ""
		}
		var exemptID = 0;
		
	/**
	  * Tworzenie obiektu count
	  *
	  * @author     Orkin (AVNTeam.net)
	  */
		this.notifications = new Notifications();
		
	/**
	  * Tworzenie obiektu count
	  *
	  * @author     Orkin (AVNTeam.net)
	  */
		this.count = new Count();
		
	/**
	  * Tworzenie obiektu channel
	  *
	  * @author     Orkin (AVNTeam.net)
	  */
		this.channel = new Channel();
		
	/**
	  * Tworzenie obiektu user
	  *
	  * @author     Orkin (AVNTeam.net)
	  */
		this.user = new User();
		
	/**
	  * Obiekt zawierający metody i ustawienia powiadomień
	  *
	  * @author     Orkin (AVNTeam.net)
	  */
		function Notifications() {
			var selfNotifications = this;
			
			var options = {
									message: "",
									status: "",
									pos: "top-right",
									timeout: 5000
								}
			
			Notifications.prototype.show = function(msg, status) {
				if (status == "success") {
					options.message = msg;
					options.status = "success";
				} else if (status == "error") {
					options.message = msg;
					options.status = "danger";
				} else {
					options.message = msg;
					options.status = "primary";
				}

				UIkit.notification(options);
			}
		}
		
	/**
	  * Obiekt zawierający metody pola Count
	  *
	  * @author     Orkin (AVNTeam.net)
	  */
		function Count() {
			var selfCount = this;
			
			Count.prototype.increase = function( id ) {
				var value = parseInt( $( "#" + id + "CountHidden" ).val() ) + 1;
				if ( value > 10 ) value = 10;
				selfCount.printCount(id, value);
			}
			
			Count.prototype.decrease = function( id ) {
				var value = parseInt( $( "#" + id + "CountHidden" ).val() ) - 1;
				if ( value < 0 ) value = 0;
				selfCount.printCount(id, value);
			}
			
			Count.prototype.printCount = function( id, value ) {
				$( "#" + id + "Count" ).html( value );
				$( "#" + id + "CountHidden" ).val( value );
				if ( value == 0 ) {
					$( "#" + id +  "Button" ).attr( "disabled", true );
				} else {
					$( "#" + id +  "Button" ).removeAttr('disabled');
				}
			}
		}
		
	/**
	  * Obiekt zawierający metody kanałów
	  *
	  * @author     Orkin (AVNTeam.net)
	  */
		function Channel() {
			var selfChannel = this;
			
			Channel.prototype.showFree = function() {
				$( '#emptyChannelList > tbody > tr' ).remove();
				$.post( "core/run.php", { command: "showEmpty", data: "" }, function( data ) {
					var jData = JSON.parse( data );
					var output = "";
					var parent = 0;
					$.each( jData, function( index ) {
						if ( index == 0 ) {
							parent = this.pid;
							$( '#emptyChannelList > tbody:last-child' ).append( '<tr><td></td><td colspan=4 class="uk-text-muted uk-text-center">Kanały Gildijne</td></tr>' );
						}
						if ( parent != this.pid ) {
							parent = this.pid;
							$( '#emptyChannelList > tbody:last-child' ).append( '<tr><td></td><td colspan=4 class="uk-text-muted uk-text-center">Kanały Prywatne</td></tr>' );
						}
						if (this.seconds_empty == -1) this.seconds_empty = 0;
						this.seconds_empty = Math.floor(this.seconds_empty/86400)+"d";
						$( '#emptyChannelList > tbody:last-child' ).append( '<tr id = "tcID' + this.cid + '"><td><input class="uk-checkbox" type="checkbox"></td><td>' + this.channel_name + '</td><td class="uk-text-center">' + this.owner + '</td><td class="uk-text-center">' + this.seconds_empty + '</td><td class="uk-text-right">' + (this.owner == "" ? '<button class="uk-button uk-button-default uk-button-small" uk-toggle="target: #modal-giveChannel" onclick="window.form.channel.yesThisChannel(' + this.cid + ')">Przydziel</button>':'<button class="uk-button uk-button-default uk-button-small" uk-toggle="target: #modal-exemptChannel" onClick="window.form.channel.yesThisChannel(' + this.cid + ')">Zwolnij</button></td></tr>') );
					} );
				} );
			}
			
			Channel.prototype.yesThisChannel = function(cid) {
				$( '#giveDefault' ).prop( 'selected', true );
				$( '#giveButton' ).attr( 'disabled', true );
				giveThisChannel.cid = cid;
				exemptID = cid;
			}
			
			Channel.prototype.yesThisChannelToThisUser = function(data) {
				console.log(data);
				data = JSON.parse(data);
				console.log(data.clid + " " + data.client_database_id + " " + data.client_nickname);
				if ( data.clid === 0 && data.client_database_id === 0 && data.client_nickname === 0 ) {
					$( '#giveButton' ).attr( 'disabled', true );
				} else {
					$( '#giveButton' ).removeAttr('disabled');
					giveThisChannel.clid = data.clid;
					giveThisChannel.cldbid = data.client_database_id;
					giveThisChannel.nick = data.client_nickname;
				}
			}
			
			Channel.prototype.yesGiveThisChannelToThisUser = function() {
				
				$.post( "core/run.php", { command: "giveChannel", data: giveThisChannel }, function( data ) {
					selfChannel.showFree();
					if (data == 1) {
						self.notifications.show("Kanał przyznany!", "success");
					} else {
						form.notifications.show( data, "error" );
					}
				} );
			}
			
			Channel.prototype.yesExemptThisChannel = function() {
				selfChannel.free();
			}
			
			Channel.prototype.free = function() {
				$.post( "core/run.php", { command: "freeChannel", data: exemptID }, function( data ) {
					selfChannel.showFree();
					if (data == 1) {
						self.notifications.show("Kanał zwolniony!", "success");
					} else {
						form.notifications.show( data, "error" );
					}
				} );
			}
		}
		

	/**
	  * Obiekt zawierający metody użytkowników
	  *
	  * @author     Orkin (AVNTeam.net)
	  */
		function User() {
			var selfUser = this;
			User.prototype.showNoChannel = function() {
				$.post( "core/run.php", { command: "showUsersNoChannel", data: "" }, function( data ) {
					// console.log(data);
					// Sortowanie alfabetycznie
					var jData = $.map(JSON.parse( data ), function(value, index) {
						return [value];
					});
					jData.sort(function(a, b) {
						return compareStrings(a.client_nickname, b.client_nickname);
					});
					$( '#selectUserForChannel:last-child' ).append( '<option id="giveDefault" value=\'{\"clid\": 0, \"client_database_id\": 0, \"client_nickname\": 0}\'>Wybierz...</option>' );
					$.each( jData, function( index ) {
						$( '#selectUserForChannel:last-child' ).append( '<option value=\'{\"clid\": ' + this.clid + ', \"client_database_id\": ' + this.client_database_id + ', \"client_nickname\": \"' + this.client_nickname + '\"}\'>' + this.client_nickname + ' (' + this.client_database_id + ') (' + this.client_unique_identifier + ')</option>' );
					} );
				} );
			}
		}
	}

var form = new Form();

$( document ).ready( function() {
	
	$( "#channelPrivCreate" ).ajaxForm( function( responseText ) {
		if ( responseText == "1" ) {
			var count = $( "#channelPrivCountHidden" ).val();
			form.notifications.show( "Pomyślnie utworzono " + count + " kanałów!", "success" );
		} else {
			form.notifications.show( responseText, "error" );
		}
		form.count.printCount( "channelPriv", 0 );
    } );
	$( "#channelClanCreate" ).ajaxForm( function( responseText ) {
		if ( responseText == "1" ) {
			var count = $( "#channelClanCountHidden" ).val();
			form.notifications.show( "Pomyślnie utworzono " + count + " kanałów!", "success" );
		} else {
			form.notifications.show( responseText, "error" );
		}
		form.count.printCount( "channelClan", 0 );
    } ); 
	
	window.form.channel.showFree();
	window.form.user.showNoChannel();
	
} );


function compareStrings(a, b) {
  // Assuming you want case-insensitive comparison
  a = a.toLowerCase();
  b = b.toLowerCase();

  return (a < b) ? -1 : (a > b) ? 1 : 0;
}