<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
$ip = $_SERVER['REMOTE_ADDR'];
if (isset($_SERVER["HTTP_CF_CONNECTING_IP"]) && !empty($_SERVER["HTTP_CF_CONNECTING_IP"])) $ip = $_SERVER["HTTP_CF_CONNECTING_IP"];
require("core/config.php");
require("core/lib/ts3admin.class.php");
$isAdmin = false;
$tsAdmin = new ts3admin($config['ip'], $config['queryport']);
if($tsAdmin->getElement("success", $tsAdmin->connect())) {
	$tsAdmin->login($config['user'], $config['pass']);
	$tsAdmin->selectServer($config['port']);
	$clients = $tsAdmin->getElement("data", $tsAdmin->clientList("-uid -away -voice -times -groups -info -country -icon -ip -badges"));
	foreach ($clients as $client => $data) {
		$clients[$client]['client_servergroups'] = explode(',', $clients[$client]['client_servergroups']);
		foreach ($clients[$client]['client_servergroups'] as $group) {
			if (in_array($group, $config['admins']) && $ip == $clients[$client]['connection_client_ip']) {
				$isAdmin = true;
				break 2;
			}
		}
	}
	if (!$isAdmin) {
		header("Location: http://www.mpcforum.pl/");
		die();
	} else {
?>
<!DOCTYPE html>
<html>
    <head>
		<meta charset="UTF-8">
        <title>Panel zarządzania serwerem</title>
        <link rel="stylesheet" href="css/uikit.min.css">
		<link rel="stylesheet" href="css/style.css">
		<script src="js/jquery-2.2.4.min.js"></script>
		<script src="js/jquery.form.min.js"></script>
        <script src="js/uikit.min.js"></script>
		<script src="js/script.js"></script>
    </head>
    <body>
		<div class="uk-grid-collapse uk-margin-large-top" uk-grid>
			<!-- Panel góra -->
			<div class="uk-width-2-3 uk-margin-auto">
				<div class="uk-background-secondary avn-navbar uk-light uk-text-center">
					ts.MPCForum.pl Panel
				</div>
			</div>
			<!-- End Panel góra -->
			
			<!-- Pod panelem -->
			<div class="uk-width-2-3 uk-margin-auto">
				<div class="uk-background-muted uk-padding">
				
					<!-- Grid treści strony -->
					<div uk-grid>
					
						<!-- Lewa -->
						<div class="uk-width-5-6">
							<!-- Ścieżka strony -->
							<div class="uk-section uk-padding-remove">
								<ul class="uk-breadcrumb">
									<li><a href="#">ts.MPCForum.pl</a></li>
									<li><span href="#">Panel</span></li>
								</ul>
							</div>
							<!-- End Ścieżka strony -->
							
							<!-- Treść -->
							<h3 class="uk-heading-line"><span>Tworzenie kanałów</span></h3>
							<div uk-grid>
								<div class="uk-width-1-1">
									<div uk-grid>
										<div class="uk-width-expand">
											Kanały prywatne
										</div>
										<div class="uk-width-auto">
											<form id="channelPrivCreate" action="core/run.php" method="POST">
												<a id="channelPrivDecrease" href="#" class="uk-icon-button uk-margin-right" onClick="form.count.decrease('channelPriv');" uk-slidenav-previous></a>
												<span id="channelPrivCount" class="uk-text-muted uk-margin-right">0</span>
												<a id="channelPrivIncrease" href="#" class="uk-icon-button" onClick="form.count.increase('channelPriv');" uk-slidenav-next></a>
												<!-- Wysyłane dane -->
												<input name="command" type="hidden" value="channelPrivCreate">
												<input id="channelPrivCountHidden" name="data" type="hidden" value=0>
												<!-- End Wysyłane dane -->
												
												<button id="channelPrivButton" class="uk-button uk-button-default uk-button-small" disabled>Utwórz</button>
											</form>
										</div>
									</div>
								</div>
								<div class="uk-width-1-1 uk-margin-remove-top">
									<div uk-grid>
										<div class="uk-width-expand">
											Kanały gildijne
										</div>
										<div class="uk-width-auto">
											<form id="channelClanCreate" action="core/run.php" method="POST">
												<a id="channelClanDecrease" href="#" class="uk-icon-button uk-margin-right" onClick="form.count.decrease('channelClan');" uk-slidenav-previous></a>
												<span id="channelClanCount" class="uk-text-muted uk-margin-right">0</span>
												<a id="channelClanIncrease" href="#" class="uk-icon-button" onClick="form.count.increase('channelClan');" uk-slidenav-next></a>
												<!-- Wysyłane dane -->
												<input name="command" type="hidden" value="channelClanCreate">
												<input id="channelClanCountHidden" name="data" type="hidden" value=0>
												<!-- End Wysyłane dane -->
												
												<button id="channelClanButton" class="uk-button uk-button-default uk-button-small" disabled>Utwórz</button>
											</form>
										</div>
									</div>
								</div>
								<div class="uk-width-1-1">
									<ul uk-accordion>
										<li>
											<h3 class="uk-accordion-title">Kanały</h3>
											<div class="uk-accordion-content uk-margin-remove-top">
												<table id="emptyChannelList" class="uk-table uk-margin-remove-top avnSlimList">
													<thead>
														<tr>
															<th class="uk-table-shrink"></th>
															<th class="uk-table-expand">Nazwa</th>
															<th class="uk-width-auto uk-text-center">Właściciel</th>
															<th class="uk-width-auto uk-text-center">Pusty od</th>
															<th class="uk-width-auto uk-text-center">Akcje</th>
														</tr>
													</thead>
													<tbody>
													</tbody>
												</table>
											</div>
										</li>
									</ul>
									<div class="result">
									
									</div>
								</div>
							</div>
							<div id="modal-giveChannel" uk-modal>
								<div class="uk-modal-dialog uk-modal-body">
									<h2 class="uk-modal-title">Nadaj kanał</h2>
									<p>
										<div class="uk-margin">
											<label class="uk-form-label" for="selectUserForChannel">Wybierz usera</label>
											<div class="uk-form-controls">
												<select class="uk-select" id="selectUserForChannel" onchange="window.form.channel.yesThisChannelToThisUser(value);">
												</select>
											</div>
										</div>
									</p>
									<p class="uk-text-right">
										<button class="uk-button uk-button-default uk-modal-close" type="button">Anuluj</button>
										<button id="giveButton" class="uk-button uk-button-primary uk-modal-close" type="button" onclick="window.form.channel.yesGiveThisChannelToThisUser();">Nadaj</button>
									</p>
								</div>
							</div>
							<div id="modal-exemptChannel" uk-modal>
								<div class="uk-modal-dialog uk-modal-body">
									<h2 class="uk-modal-title">Czy na pewno chcesz zwolnić kanał?</h2>
									<p class="uk-text-right">
										<button class="uk-button uk-button-default uk-modal-close" type="button">Anuluj</button>
										<button class="uk-button uk-button-danger uk-modal-close" type="button" onclick="window.form.channel.yesExemptThisChannel(); disabled">Zwolnij</button>
									</p>
								</div>
							</div>
							<!-- End Treść -->
							
						</div>
						<!-- End Lewa -->
						
						<!-- Prawa -->
						<div class="uk-width-1-6">
							<div class="uk-placeholder uk-text-center">TUTAJ BĘDZIE STATUS USŁUG</div>
						</div>
						<!-- End Prawa -->
						
					</div>
					<!-- End Grid treści strony -->
					
				</div>
			</div>
			<!-- End Pod panelem -->
			
		</div>
    </body>
</html>
<?php
	}
}
?>