<?php
/*
    AVNBot
    Copyright (C) 2016  Orkin (AVNTeam.net)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
	
*/

#Brak limitów czasu wykonania kodu
set_time_limit(0);

#Podłączanie ts3admin.class.php
require('class/ts3admin.class.php');

#Podłączanie config.php
require('config.php');

#Podłączanie functions.php
require('functions.php');

#Budowanie nowego obiektu
$tsAdmin = new ts3admin($config['server']['ip'], $config['server']['queryport']);

#Sprawdzanie połączenia z serwerem
if($tsAdmin->getElement('success', $tsAdmin->connect())) {
	
		#Logowanie się na użytkownika Query
		$tsAdmin->login($config['query']['login'], $config['query']['password']);
		
		#Wybieranie serwera
		$tsAdmin->selectServer($config['server']['port']);
		
		#Ustawianie nazwy bota
		$tsAdmin->setName($config['bot']['nickname'].' Stats');

		
		echo "Connection established!\n";

		$mysqli = new mysqli($config['db']['host'], $config['db']['login'], $config['db']['password'], $config['db']['db_name']);
		
		if ($mysqli->connect_error) {
				die('Error: ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
		}
		
		#Data wykonania pętli
		date_default_timezone_set('Europe/Warsaw');
		$datapetli = date('Y-m-d G:i:s');
		$datapetli2 = date('Y-m-d');
		$wczoraj = date('Y-m-d', strtotime('yesterday'));
		
		/* CLIENTS */
		$allclients = $tsAdmin->getElement('data', $tsAdmin->clientList());
		foreach($allclients as $oneclient) {
				if($oneclient['client_type'] == 0) {
						$client = $tsAdmin->getElement('data', $tsAdmin->clientInfo($oneclient['clid']));
						$result = $mysqli->query("SELECT time_online, time_afk, time_online_record, pkt_added, rep, time_rep_added, afk_rep_added, last_online, last_afk, last_scanned FROM avnbot_clients WHERE cldbid = ".$client['client_database_id']);
						if($result->num_rows > 0){							
								// jeśli data pętli - data ostatniego sprawdzenia jest mniejsza niż jego aktualny czas online, to uwzględniaj last_online itp
								// jeśli jest większa, to nie uwzględniaj
								$dbclient = $result->fetch_array();
								$client['rep'] = $dbclient['rep'];
								$client['pkt_added'] = $dbclient['pkt_added'];
								$client['online_time_stat'] = floor($client['connection_connected_time']/1000);
								$client['idle_time_stat'] = floor($client['client_idle_time']/1000) - $config['module']['statistics']['afk_time'];
								if($client['idle_time_stat'] < 0) $client['idle_time_stat'] = 0;

								if(juzmozna($datapetli, $dbclient['last_scanned'], $client['online_time_stat']) == false) {
										//uwzględniamy
										$client['online_time_stat_hour'] = floor($dbclient['time_online']/3600);
										$client['afk_time_stat_hour'] = floor($dbclient['time_afk']/3600);
										$client['online_time_stat_hour_2'] = $client['online_time_stat_hour'] - $dbclient['time_rep_added'];
										$client['afk_time_stat_hour_2'] = $client['afk_time_stat_hour'] - $dbclient['afk_rep_added'];
										if($client['online_time_stat_hour_2'] >= 1) {
												$client['rep'] = $client['rep'] + ($client['online_time_stat_hour_2']*$config['module']['rep']['hour_pkt']);
												$dbclient['time_rep_added'] = $dbclient['time_rep_added'] + $client['online_time_stat_hour_2'];
										}
										if($client['afk_time_stat_hour_2'] >= 1) {
												$client['rep'] = $client['rep'] - ($client['afk_time_stat_hour_2']*($config['module']['rep']['afk_pkt'] + $config['module']['rep']['hour_pkt']));
												$dbclient['afk_rep_added'] = $dbclient['afk_rep_added'] + $client['afk_time_stat_hour_2'];
										}
										
										
										if(($client['online_time_stat'] > $dbclient['time_online_record']) && ($client['online_time_stat'] > 3600)) {
												$client['online_time_record'] = $client['online_time_stat'];
												if($dbclient['pkt_added'] == 0) {
														$client['rep'] = $client['rep'] + $config['module']['rep']['solorecord_pkt'];
														$client['pkt_added'] = 1;
												}
										} else {
												$client['online_time_record'] = $dbclient['time_online_record'];
										}
										$client['online_time_stat_suma'] = $dbclient['time_online'] + ($client['online_time_stat'] - $dbclient['last_online']);
										$client['idle_time_stat_uusuma'] = $client['idle_time_stat'] - $dbclient['last_afk'];
										if($client['idle_time_stat_uusuma'] < 0) $client['idle_time_stat_uusuma'] = 0;
										$client['idle_time_stat_suma'] = $dbclient['time_afk'] + $client['idle_time_stat_uusuma'];
										$results = $mysqli->query("UPDATE avnbot_clients SET time_online = ".$client['online_time_stat_suma'].", time_afk = ".$client['idle_time_stat_suma'].", time_online_record = ".$client['online_time_record'].", pkt_added = ".$client['pkt_added'].", rep = ".$client['rep'].", time_rep_added = ".$dbclient['time_rep_added'].", afk_rep_added = ".$dbclient['afk_rep_added'].", last_online = ".$client['online_time_stat'].", last_afk = ".$client['idle_time_stat'].", last_scanned = '".$datapetli."' WHERE cldbid = ".$client['client_database_id']);
								} else {
										//nie uwzględniamy
										if(($client['online_time_stat'] > $dbclient['time_online_record']) && ($client['online_time_stat'] > 3600)) {
												$client['online_time_record'] = $client['online_time_stat'];
												if($dbclient['pkt_added'] == 0) {
														$client['rep'] = $client['rep'] + $config['module']['rep']['solorecord_pkt'];
														$client['pkt_added'] = 1;
												}
										} else {
												$client['online_time_record'] = $dbclient['time_online_record'];
												$client['pkt_added'] = 0;
										}
										$client['online_time_stat_suma'] = $dbclient['time_online'] + $client['online_time_stat'];
										$client['idle_time_stat_suma'] = $dbclient['time_afk'] + $client['idle_time_stat'];
										$results = $mysqli->query("UPDATE avnbot_clients SET time_online = ".$client['online_time_stat_suma'].", time_afk = ".$client['idle_time_stat_suma'].", time_online_record = ".$client['online_time_record'].", pkt_added = ".$client['pkt_added'].", rep = ".$client['rep'].", last_online = ".$client['online_time_stat'].", last_afk = ".$client['idle_time_stat'].", last_scanned = '".$datapetli."' WHERE cldbid = ".$client['client_database_id']);
								}

								unset($results);
								unset($dbclient);
						} else {
								$client['rep'] = 0;
								$client['online_time_stat'] = floor($client['connection_connected_time']/1000);
								$client['idle_time_stat'] = floor($client['client_idle_time']/1000) - $config['module']['statistics']['afk_time'];
								if($client['idle_time_stat'] < 0) $client['idle_time_stat'] = 0;
								
								$insert_row = $mysqli->query("INSERT INTO avnbot_clients (cldbid, time_online, time_afk, time_online_record, pkt_added, date_connect_added, rep, time_rep_added, afk_rep_added, last_online, last_afk, last_scanned) VALUES (".$client['client_database_id'].", ".$client['online_time_stat'].", ".$client['idle_time_stat'].", ".$client['online_time_stat'].", 1, '".$datapetli2."', ".$client['rep'].", 0, 0, ".$client['online_time_stat'].", ".$client['idle_time_stat'].", '".$datapetli."')");
								unset($insert_row);
						}
						
						unset($result);
				}
		}
		
		/* CHANNELS */
		if($config['module']['channelcheck']['enable'] == true) {
				if($config['module']['channelcheck']['autodel'] == false) {
						$channeldelist = '[SIZE='.$config['module']['adminsonline']['groupsize'].'][B][COLOR='.$config['module']['adminsonline']['groupcolor'].']Kanały do usunięcia[/COLOR][/B][/SIZE]\n';
				}
				
				$allchannels = $tsAdmin->getElement('data', $tsAdmin->channelList());
				$allusers = $tsAdmin->getElement('data', $tsAdmin->clientList());
				$channels['start'] = 0;
				foreach($allchannels as $channel) {
						$channel['admins'] = array();
						if($channels['start'] == 0) {
								if($channel['cid'] == $config['module']['channelcheck']['first']) {
										$channels['start'] = 1;
								}
						} elseif($channels['start'] == 1) {
								if($channel['cid'] == $config['module']['channelcheck']['last']) {
										$channels['start'] = 0;
										break;
								} else {
										if($channel['pid'] == 0) {
												$result = $mysqli->query("SELECT owner_cldbid, date, rep, last_scanned FROM avnbot_channels WHERE cid = ".$channel['cid']);
												if($result->num_rows > 0) {
														//jeżeli jest w bazie
														$dbchannel = $result->fetch_array();
														$alladmins = $tsAdmin->getElement('data', $tsAdmin->channelGroupClientList($cid = $channel['cid'], $cldbid = NULL, $cgid = $config['module']['channelcheck']['admin']));
														$admini = '';
														if(!empty($alladmins)) {
																$i = 0;
																foreach($alladmins as $admin) {
																		if($i == 0) {
																				$admini = $admin['cldbid'];
																				$i = 1;
																		} else {
																			$admini .= ','.$admin['cldbid'];
																		}
																		array_push($channel['admins'], $admin['cldbid']);
																}
																//Tutaj aktualizacja adminów
																$i = 0;
														} else {
																$admini = '0';
														}
														$results = $mysqli->query("UPDATE avnbot_channels SET owner_cldbid = '".$admini."' WHERE cid = ".$channel['cid']);
														if($config['module']['channelcheck']['mode'] == 1) {
																//sprawdzanie czy ktoś jest
																//jak jest ktoś to sprawdza date_added i jak jest więcej niż dzień, to zmienia datę i dodaje punkty reputacji
																//jak doda punkty to ustawia pkt_added na 1 i date_added na aktualną datę
																//jak nie ma kolesia, to odejmuje pkt, ustawia pkt_added na 1 i date_added na aktualną datę
																//jak po odjęciu rep jest mniejszy lub równy 0 to usuwa kanał (ale na razie zakomentuj to na czas testów reszty)
																//jak po dodaniu rep jest więszy niż maksymalna wartość z configa, to ustaw na maksymalną watrość
																if($channel['total_clients'] > 0) {
																		if(juzmozna($datapetli, $dbchannel['date'], 86400) == true) {
																				$channel['startrep'] = $config['module']['channelcheck']['daystodel']*10;
																				if($dbchannel['rep'] < $channel['startrep']) {
																						$dbchannel['rep'] = $channel['startrep'];
																				} else {
																						$dbchannel['rep'] = $dbchannel['rep'] + $config['module']['channelcheck']['pktperday'];
																						if($dbchannel['rep'] > $config['module']['channelcheck']['maxrep']) $dbchannel['rep'] = $config['module']['channelcheck']['maxrep'];
																				}
																				$results = $mysqli->query("UPDATE avnbot_channels SET date = '".$datapetli2."', rep = ".$dbchannel['rep']." WHERE cid = ".$channel['cid']);
																		}
																} else {
																		if(juzmozna($datapetli, $dbchannel['date'], 172800) == true) {
																				$dbchannel['rep'] = $dbchannel['rep'] - 10;
																				if($dbchannel['rep'] <= 9) {
																						//usuwanie kanału z serwera i z bazy
																						if($config['module']['channelcheck']['autodel'] == true) {
																								$tsAdmin->channelDelete($channel['cid'], $force = 1);
																								$results = $mysqli->query("DELETE FROM avnbot_channels WHERE cid = ".$channel['cid']);
																						} else {
																								$channeldelist .= '[SIZE='.$config['module']['adminsonline']['statusize'].'][B][color=#0012B5][url=channelid://'.$channel['cid'].']'.$channel['channel_name'].'[/url][/color][/B][/SIZE]\n';
																						}
																				} else {
																						$results = $mysqli->query("UPDATE avnbot_channels SET date = '".$wczoraj."', rep = ".$dbchannel['rep']." WHERE cid = ".$channel['cid']);
																				}
																		}
																}
														} else {
																//sprawdzanie czy jest admin
																//jak jest admin kanału to sprawdza date_added i jak jest więcej niż dzień, to zmienia datę i dodaje punkty reputacji
																//jak doda punkty to ustawia pkt_added na 1 i date_added na aktualną datę
																//jak nie ma kolesia, to odejmuje pkt, ustawia pkt_added na 1 i date_added na aktualną datę
																//jak po odjęciu rep jest mniejszy lub równy 0 to usuwa kanał (ale na razie zakomentuj to na czas testów reszty)
																//jak po dodaniu rep jest więszy niż maksymalna wartość z configa, to ustaw na maksymalną watrość
																$jestadmin = false;
																foreach($allusers as $user) {
																		if($user['cid'] == $channel['cid']) {
																				foreach($channel['admins'] as $myadmin) {
																						if($user['client_database_id'] == $myadmin) {
																								$jestadmin = true;
																								break;
																						}
																				}
																		}
																}
																if($jestadmin == true) {
																		if(juzmozna($datapetli, $dbchannel['date'], 86400) == true) {
																				$channel['startrep'] = $config['module']['channelcheck']['daystodel']*10;
																				if($dbchannel['rep'] < $channel['startrep']) {
																						$dbchannel['rep'] = $channel['startrep'];
																				} else {
																						$dbchannel['rep'] = $dbchannel['rep'] + $config['module']['channelcheck']['pktperday'];
																						if($dbchannel['rep'] > $config['module']['channelcheck']['maxrep']) $dbchannel['rep'] = $config['module']['channelcheck']['maxrep'];
																				}
																				$results = $mysqli->query("UPDATE avnbot_channels SET owner_cldbid = '".$admini."', date = '".$datapetli2."', rep = ".$dbchannel['rep']." WHERE cid = ".$channel['cid']);
																		}
																} else {
																		if(juzmozna($datapetli, $dbchannel['date'], 172800) == true) {
																				$dbchannel['rep'] = $dbchannel['rep'] - 10;
																				if($dbchannel['rep'] <= 9) {
																						//usuwanie kanału z serwera i z bazy
																						if($config['module']['channelcheck']['autodel'] == true) {
																								$tsAdmin->channelDelete($channel['cid'], $force = 1);
																								$results = $mysqli->query("DELETE FROM avnbot_channels WHERE cid = ".$channel['cid']);
																						} else {
																								$channeldelist .= '[SIZE='.$config['module']['adminsonline']['statusize'].'][B][color=#0012B5][url=channelid://'.$channel['cid'].']'.$channel['channel_name'].'[/url][/color][/B][/SIZE]\n';
																						}
																				} else {
																						$results = $mysqli->query("UPDATE avnbot_channels SET owner_cldbid = '".$admini."', date = '".$wczoraj."', rep = ".$dbchannel['rep']." WHERE cid = ".$channel['cid']);
																				}
																		}
																}
														}
														$results = $mysqli->query("UPDATE avnbot_channels SET last_scanned = '".$datapetli."' WHERE cid = ".$channel['cid']);
														unset($results);
												} else {
														$alladmins = $tsAdmin->getElement('data', $tsAdmin->channelGroupClientList($cid = $channel['cid'], $cldbid = NULL, $cgid = $config['module']['channelcheck']['admin']));
														$admini = '';
														if(!empty($alladmins)) {
																$i = 0;
																foreach($alladmins as $admin) {
																		if($i == 0) {
																				$admini = $admin['cldbid'];
																				$i = 1;
																		} else {
																			$admini .= ','.$admin['cldbid'];
																		}
																}
																$i = 0;
														} else {
																$admini = '0';
														}
														$channel['startrep'] = $config['module']['channelcheck']['daystodel']*10;
														$insert_row = $mysqli->query("INSERT INTO avnbot_channels (cid, owner_cldbid, date, rep, last_scanned) VALUES (".$channel['cid'].", '".$admini."', '".$datapetli2."', ".$channel['startrep'].", '".$datapetli."')");
														unset($insert_row);
												}
												unset($result);
										}
								}
						}
				}
				if($config['module']['channelcheck']['autodel'] == false) {
						#Sprawdza czy status zmienił się od ostatniego sprawdzania
						$channelInfoStatus = $tsAdmin->getElement('data', $tsAdmin->channelInfo($config['module']['channelcheck']['channel']));
						if(!strcmp($channelInfoStatus['channel_description'], $channeldelist) == 0) {
								$tsAdmin->channelEdit($config['module']['channelcheck']['channel'], array('channel_description' => $channeldelist));
						}
				}
				
		}
		
		if ($mysqli->connect_error) {
				die('Error: ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
		}
		$mysqli->close();

		
} else {

		echo "\e[0;31mConnection could not be established.\e[0m\n";
}

?>