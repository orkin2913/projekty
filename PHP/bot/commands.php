<?php
/*
    AVNBot
    Copyright (C) 2016  Orkin (AVNTeam.net)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
	
*/

#Brak limitów czasu wykonania kodu
set_time_limit(0);

#Podłączanie ts3admin.class.php
require('class/ts3admin.class.php');

#Podłączanie config.php
require('config.php');


#Budowanie nowego obiektu
$tsAdmin = new ts3admin($config['server']['ip'], $config['server']['queryport']);

#Sprawdzanie połączenia z serwerem
if($tsAdmin->getElement('success', $tsAdmin->connect())) {
	
		#Logowanie się na użytkownika Query
		$tsAdmin->login($config['query']['login'], $config['query']['password']);
		
		#Wybieranie serwera
		$tsAdmin->selectServer($config['server']['port']);
		
		#Ustawianie nazwy bota
		$tsAdmin->setName($config['bot']['nickname'].'-Commands');
		
		#Przenoszenie bota do wybranego kanału
		$whoami = $tsAdmin->getElement('data', $tsAdmin->whoAmI());
		$tsAdmin->clientMove($whoami['client_id'] , $config['bot']['channel']);
		
		echo "Connection established!\n";

		$commandsrecived = $tsAdmin->getElement('data', $tsAdmin->execOwnCommand(3, 'servernotifyregister event=textprivate'));
		
		
		#Pętla z funkcjami bota
		$i['pingpong'] = 0;
		while(true)
		{

	
				$socket = stream_socket_server("tcp://127.0.0.1:61000", $errno, $errstr);
				if (!$socket) {
						echo "$errstr ($errno)<br />\n";
				} else {
						while ($conn = stream_socket_accept($socket)) {
								sleep(1000);
								fwrite($conn, 'The local time is ' . date('n/j/Y g:i a') . "\n");
								fclose($conn);
						}
						fclose($socket);
				}
				
				time_nanosleep(floor($config['bot']['speed']/10), $config['bot']['speed']%10*100000000);
				
				echo $tsAdmin->checkCommands();

				
				
		}
		
		
	
} else {

		echo "\e[0;31mConnection could not be established.\e[0m\n";
}

?>