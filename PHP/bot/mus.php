<?php
/*
    AVNBot
    Copyright (C) 2016  Orkin (AVNTeam.net)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
	
*/

#Brak limitów czasu wykonania kodu
set_time_limit(0);

#Strefa czasowa
date_default_timezone_set('Europe/Warsaw');

#Podłączanie ts3admin.class.php
require('class/ts3admin.class.php');

#Podłączanie config.php
require('config.php');

#Podłączanie functions.php
require('functions.php');

require('class/sinusbot.class.php');


$sinusbot = new SinusBot($config['module']['musicbot']['address']);
$sinusbot->login($config['module']['musicbot']['login'], $config['module']['musicbot']['password']);
$song = $sinusbot->getStatus('0a2e5eb3-b722-48dc-b88c-6fdb1fc1f495');

$title = '[SIZE='.$config['module']['musicbot']['title']['size'].'][B][COLOR='.$config['module']['musicbot']['title']['color'].']'.$config['module']['musicbot']['title']['text']."[/COLOR][/B][/SIZE]\n";
$line_n = '[SIZE='.$config['module']['musicbot']['date']['size'].'][B][COLOR='.$config['module']['musicbot']['date']['color'].']'.date('G:i:s').'[/COLOR][/B][/SIZE] '.'[SIZE='.$config['module']['musicbot']['song']['size'].'][B][COLOR='.$config['module']['musicbot']['song']['color'].']'.$song['currentTrack']['title']."[/COLOR][/B][/SIZE]\n";

$desc = $title;
$desc .= $line_n;

$config['module']['musicbot']['max']+=2;
$file = 'tmp/songs.txt';
$linecount = 0;
$handle = fopen($file, "r");
while(!feof($handle)){
		$line = fgets($handle);
		$linecount++;
}
fclose($handle);

$contents = file($file);
if($linecount >= $config['module']['musicbot']['max']) {
		unset($contents[0]); unset($contents[count($contents)]);
		for($i = 1; $i <= count($contents); $i++) {
				$desc .= $contents[$i];
		}

} else {
		unset($contents[0]);
		for($i = 1; $i <= count($contents); $i++) {
				$desc .= $contents[$i];
		}
}


$sprawdz[1] = explode('[/COLOR][/B][/SIZE]', $line_n);
$sprawdz[2] = explode('[/COLOR][/B][/SIZE]', $contents[1]);

if($sprawdz[1][1] != $sprawdz[2][1]) {
		file_put_contents($file, $desc);
		
		#Budowanie nowego obiektu
		$tsAdmin = new ts3admin($config['server']['ip'], $config['server']['queryport']);

		#Sprawdzanie połączenia z serwerem
		if($tsAdmin->getElement('success', $tsAdmin->connect())) {
			
				#Logowanie się na użytkownika Query
				$tsAdmin->login($config['query']['login'], $config['query']['password']);
				
				#Wybieranie serwera
				$tsAdmin->selectServer($config['server']['port']);
				
				#Ustawianie nazwy bota
				$tsAdmin->setName($config['bot']['nickname'].' - Status');
				
				$tsAdmin->channelEdit($config['module']['musicbot']['channel'], array('channel_description' => $desc));
			
		} else {

				echo "\e[0;31mConnection could not be established.\e[0m\n";
		}
}


?>