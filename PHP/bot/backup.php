<?php
/*
    AVNBot
    Copyright (C) 2016  Orkin (AVNTeam.net)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
	
*/

#Podłączanie config.php
require('config.php');
date_default_timezone_set('Europe/Warsaw');
$date = date('Y-m-d G:i:s');
if(substr($config['module']['backup']['backupfolder'], -1) != '/') {
		$config['module']['backup']['backupfolder'] = $config['module']['backup']['backup'].'/';
}
$backup_name = 'ts3server.sqlitedb.'.date('Y-m-d_H-i-s').'.bak';

if($config['module']['backup']['protect'] == false) {
		copy($config['module']['backup']['path'], $backup_name);
} else {
		copy($config['module']['backup']['path'], 'ts3server.sqlitedb');
		echo system('zip -P '.$config['module']['backup']['password'].' '.$backup_name.'.zip ts3server.sqlitedb');
		unlink('ts3server.sqlitedb');
}

if($config['module']['backup']['backuptofolder'] == true) {
		#Usuwanie starych kopii
		if($config['module']['backup']['deleteold'] == true) {
				if($handle = opendir($config['module']['backup']['backupfolder'])) {
						while(false !== ($file = readdir($handle))) {
								if((time()-filectime($config['module']['backup']['backupfolder'].$file)) >= $config['module']['backup']['oldtime']) {  
										if((preg_match('/\.bak$/i', $file)) || (preg_match('/\.bak.zip$/i', $file))) {
												unlink($config['module']['backup']['backupfolder'].$file);
										}
								}
						}
				}
		}
		if($config['module']['backup']['protect'] == false) {
				copy($backup_name, $config['module']['backup']['backupfolder'].$backup_name);
		} else {
				copy($backup_name.'.zip', $config['module']['backup']['backupfolder'].$backup_name.'.zip');
		}
}

if($config['module']['backup']['backuptoftp'] == true) {
		$pass = explode(';', $config['module']['backup']['ftppass']);

		$ftp_conn = ftp_connect($pass[0], 21) or die('Could not connect to '.$pass[0]);
		$login = ftp_login($ftp_conn, $pass[1], $pass[2]);
		if($config['module']['backup']['protect'] == false) {
				if(ftp_put($ftp_conn, $backup_name, $backup_name, FTP_ASCII)) {
						echo 'Successfully uploaded '.$backup_name;
				} else {
						echo 'Error uploading '.$backup_name;
				}
		} else {
				if(ftp_put($ftp_conn, $backup_name.'.zip', $backup_name.'.zip', FTP_ASCII)) {
						echo 'Successfully uploaded '.$backup_name.'.zip';
				} else {
						echo 'Error uploading '.$backup_name.'.zip';
				}
		}
		
		// close connection
		ftp_close($ftp_conn);
}

if($config['module']['backup']['protect'] == false) {
		unlink($backup_name);
} else {
		unlink($backup_name.'.zip');
}
?>