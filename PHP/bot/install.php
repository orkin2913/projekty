<?php
/*
    AVNBot
    Copyright (C) 2016  Orkin (AVNTeam.net)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
	
*/

#Brak limitów czasu wykonania kodu
set_time_limit(0);

#Podłączanie ts3admin.class.php
require('class/ts3admin.class.php');

#Podłączanie config.php
require('config.php');

#Podłączanie functions.php
require('functions.php');

echo "\n";

logo();

echo "Checking version...\n";
$version = str_replace(array("\t", "\n"), "", checkversion());

if($version != $config['bot']['version']) {
	echo "\e[0;33mWarning!\e[0m\nLatest version: \e[0;32m".$version."\e[0m\nYour version: \e[0;31m".$config['bot']['version']."\e[0m\nWe recommend update\n\n";
	
} else {
	echo "\e[0;32mOK!\e[0m - Version up to date\n\n";
}





#Budowanie nowego obiektu
$tsAdmin = new ts3admin($config['server']['ip'], $config['server']['queryport']);

#Sprawdzanie połączenia z serwerem
if($tsAdmin->getElement('success', $tsAdmin->connect())) {
	
		#Logowanie się na użytkownika Query
		$tsAdmin->login($config['query']['login'], $config['query']['password']);
		
		#Wybieranie serwera
		$tsAdmin->selectServer($config['server']['port']);
		
		#Ustawianie nazwy bota
		$tsAdmin->setName($config['bot']['nickname'].' - Install');
		
		#Przenoszenie bota do wybranego kanału
		$whoami = $tsAdmin->getElement('data', $tsAdmin->whoAmI());
		$tsAdmin->clientMove($whoami['client_id'] , $config['bot']['channel']);
		
		echo "Connection established!\n";

		$clients['aktualnie'] = listaclientow();
		if($config['module']['accessgroup']['enable'] == true) {
				$clients['aktualnie2'] = listaclientow();
		}
		#$commandsrecived = $tsAdmin->getElement('data', $tsAdmin->execOwnCommand(3, 'servernotifyregister event=textprivate'));
		
		echo "Instalowanie grup dostępowych...\n";
		accessgroupinstall();
		
		
		
	
} else {

		echo "\e[0;31mConnection could not be established.\e[0m\n";
}

?>